# sa-admin-monomer

#### 介绍

[sa-admin-ui](https://gitee.com/wlf213/sa-admin-ui)和[sa-admin-server](https://gitee.com/wlf213/sa-admin-server)的合在一起的单体应用，并且去除Redis相关依赖配置，仅需Mysql即可启动。

#### 软件架构
springboot+mybatis-plus+sa-token

- 主体框架springboot2.7，包括基本的MVC框架，Cache集成，以及定时任务等。
- 持久层用了Mybatis-Plus，Mybatis用习惯了，用这个没有什么门槛，自由度高，功能强大。
- sa-token登录鉴权框架，API简洁，功能强大。门槛较低。

#### 演示地址

[演示地址](http://39.105.152.194:8011/)


#### 环境说明
1. springboot2.7.8
2. JDK17(~~最低JDK11~~)


#### 使用说明
- 单体项目直接启动即可，仅需Mysql即可启动。
- 此项目本质其实还是前后台分离，并未使用后端模板引擎，只是为了减少一点入门成本。
- 实际上就是把前端项目放进了后端项目中，单体项目，缺点:：无法考虑后台服务的扩展。比如负载均衡等等，优点：开发部署成本较低。
1. clone 项目
2. 新建数据库sa_server
3. ~~运行doc中的sql文件~~，新版本已经不需要。
4. 修改数据库连接配置
5. 启动springboot项目，数据表以及初始数据会自动生成。不报错的情况下服务就已经启动了。
6. 项目启动默认首页：http://localhost:8100/sa/page/index.html

