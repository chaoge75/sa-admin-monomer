package com.wlf.server;

import cn.hutool.core.io.FileUtil;
import com.wlf.server.common.entity.SysMenu;
import com.wlf.server.common.service.SysMenuService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

@SpringBootTest
@ActiveProfiles("dev")
@AutoConfigureMockMvc
class ServerApplicationTests {

    //mock对象
    @Resource
    private MockMvc mockMvc;
    @Resource
    private SysMenuService menuService;

    @Test
    void contextLoads() {
        System.out.println(mockMvc);
    }

    @Test
    public void bookApiTest() {
        for (SysMenu menu : menuService.list()) {
            System.out.println(menu);
        }
    }

    public static void main(String[] args) {
        System.out.println(FileUtil.getTmpDirPath());
    }
}
