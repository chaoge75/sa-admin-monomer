-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: sa_server
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `dept_user`
--

DROP TABLE IF EXISTS `dept_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dept_user` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='部门用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dept_user`
--

LOCK TABLES `dept_user` WRITE;
/*!40000 ALTER TABLE `dept_user` DISABLE KEYS */;
INSERT INTO `dept_user` VALUES ('1571073810394898434','1530857401958273026','1571073304574418946'),('1615163799352926209','1530857401958273026','1526923684416102402'),('1615164724859658242','1530857401958273026','1538457626805809153'),('1615955592742977539','1530856961204031490','1571073778639822849');
/*!40000 ALTER TABLE `dept_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `im_chat_record`
--

DROP TABLE IF EXISTS `im_chat_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `im_chat_record` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `send_user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发送消息人ID',
  `receive_user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '接收消息人ID',
  `msg_body` json DEFAULT NULL COMMENT '消息主体',
  `send_time` datetime DEFAULT NULL COMMENT '消息发送时间',
  `reading` tinyint(1) DEFAULT '0' COMMENT '是否已读',
  PRIMARY KEY (`id`),
  KEY `sys_im_log_send_user_id_IDX` (`send_user_id`) USING BTREE,
  KEY `sys_im_log_receive_user_id_IDX` (`receive_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='聊天记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `im_chat_record`
--

LOCK TABLES `im_chat_record` WRITE;
/*!40000 ALTER TABLE `im_chat_record` DISABLE KEYS */;
INSERT INTO `im_chat_record` VALUES ('1600101513412247554','1538457626805809153','1526923684416102402','{\"msgType\": 0, \"msgContent\": \"你好世界\"}','2022-12-06 20:15:04',1),('1600101617430986754','1526923684416102402','1538457626805809153','{\"msgType\": 0, \"msgContent\": \"已阅读\"}','2022-12-06 20:15:29',1);
/*!40000 ALTER TABLE `im_chat_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice_user`
--

DROP TABLE IF EXISTS `notice_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notice_user` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `notice_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公告ID',
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID',
  `read_time` datetime DEFAULT NULL COMMENT '已读时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `notice_user_notice_id_IDX` (`notice_id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='通知用户关联表,用来区分已读和未读';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice_user`
--

LOCK TABLES `notice_user` WRITE;
/*!40000 ALTER TABLE `notice_user` DISABLE KEYS */;
INSERT INTO `notice_user` VALUES ('1601221371440979970','1600106270579245057','1526923684416102402','2022-12-09 22:24:59'),('1604821786757812227','1604821786757812226','1526923684416102402','2022-12-19 20:51:45');
/*!40000 ALTER TABLE `notice_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organ_user`
--

DROP TABLE IF EXISTS `organ_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organ_user` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `organ_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organ_user_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机构用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organ_user`
--

LOCK TABLES `organ_user` WRITE;
/*!40000 ALTER TABLE `organ_user` DISABLE KEYS */;
INSERT INTO `organ_user` VALUES ('1571073810331983874','1529812318349697026','1571073304574418946'),('1615163799218708481','1529812318349697026','1526923684416102402'),('1615164724754800641','1529812318349697026','1538457626805809153'),('1615955592680062978','1529827122216984577','1571073778639822849');
/*!40000 ALTER TABLE `organ_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_user`
--

DROP TABLE IF EXISTS `post_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post_user` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_user_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='岗位用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_user`
--

LOCK TABLES `post_user` WRITE;
/*!40000 ALTER TABLE `post_user` DISABLE KEYS */;
INSERT INTO `post_user` VALUES ('1571073810361344002','1530867099335933953','1571073304574418946'),('1615163799285817345','1530867099335933953','1526923684416102402'),('1615164724805132289','1530867099335933953','1538457626805809153'),('1615955592742977538','1530866900618199041','1571073778639822849');
/*!40000 ALTER TABLE `post_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_menu`
--

DROP TABLE IF EXISTS `role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_menu` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `role_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_menu_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_menu`
--

LOCK TABLES `role_menu` WRITE;
/*!40000 ALTER TABLE `role_menu` DISABLE KEYS */;
INSERT INTO `role_menu` VALUES ('1606680797262196737','1526547919887429634','1606670446193860610'),('1606680797266391042','1526547919887429634','1606670519074086913'),('1606680797266391043','1526547919887429634','1606670637227630593'),('1606680797266391044','1526547919887429634','1606670759877468161'),('1606680797266391045','1526547919887429634','1527292472508583937'),('1606680797266391046','1526547919887429634','1527292604293615618'),('1606680797266391047','1526547919887429634','1527542136449269761'),('1606680797266391048','1526547919887429634','1528560593489068034'),('1606680797266391049','1526547919887429634','1606675177129717762'),('1606680797266391050','1526547919887429634','1606675494235877377'),('1606680797266391051','1526547919887429634','1606680007504113666'),('1606680797266391052','1526547919887429634','1606680121350107137'),('1615916279099592706','1524721679870304257','1606670446193860610'),('1615916279107981313','1524721679870304257','1606670519074086913'),('1615916279112175617','1524721679870304257','1606670637227630593'),('1615916279112175618','1524721679870304257','1606670759877468161'),('1615916279112175619','1524721679870304257','1612074880809893890'),('1615916279124758530','1524721679870304257','1612074984715386881'),('1615916279128952833','1524721679870304257','1527292472508583937'),('1615916279133147138','1524721679870304257','1527292604293615618'),('1615916279133147139','1524721679870304257','1527542136449269761'),('1615916279133147140','1524721679870304257','1528560593489068034'),('1615916279141535745','1524721679870304257','1606675177129717762'),('1615916279141535746','1524721679870304257','1606675494235877377'),('1615916279141535747','1524721679870304257','1606680007504113666'),('1615916279141535748','1524721679870304257','1606680121350107137'),('1615916279141535749','1524721679870304257','1606680270298230786'),('1615916279141535750','1524721679870304257','1606680437172809729'),('1615916279149924354','1524721679870304257','1567889503241162754'),('1615916279158312961','1524721679870304257','1606605956072914945'),('1615916279162507265','1524721679870304257','1606681209289650178'),('1615916279162507266','1524721679870304257','1606681300448653313'),('1615916279162507267','1524721679870304257','1606681372733288449'),('1615916279162507268','1524721679870304257','1606681453922430978'),('1615916279162507269','1524721679870304257','1606692704559828993'),('1615916279162507270','1524721679870304257','1606693094537826305'),('1615916279162507271','1524721679870304257','1606702769446637569'),('1615916279170895874','1524721679870304257','1606703054365708290'),('1615916279170895875','1524721679870304257','1606704387885944834'),('1615916279170895876','1524721679870304257','notice-add'),('1615916279175090178','1524721679870304257','1564258642717343746');
/*!40000 ALTER TABLE `role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `role_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '角色ID',
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_menu_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES ('1571073810298429441','1524721679870304257','1571073304574418946'),('1615163799159988226','1524721679870304257','1526923684416102402'),('1615164724700274689','1524721679870304257','1538457626805809153'),('1615955592558428162','1524721679870304257','1571073778639822849');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_job`
--

DROP TABLE IF EXISTS `schedule_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule_job` (
  `id` char(19) NOT NULL COMMENT '任务id',
  `name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(1) DEFAULT '1' COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(20) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='定时任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_job`
--

LOCK TABLES `schedule_job` WRITE;
/*!40000 ALTER TABLE `schedule_job` DISABLE KEYS */;
INSERT INTO `schedule_job` VALUES ('1560590553932980225','666','TASK_commonTask','Y','*/5 * * * * ?',1,NULL,'2022-08-19 19:32:37',NULL,NULL,NULL,0),('1597922030789869570','777','TASK_errorTask','N','*/5 * * * * ?',1,NULL,'2022-11-30 19:54:35',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `schedule_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_log`
--

DROP TABLE IF EXISTS `schedule_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule_log` (
  `id` char(19) NOT NULL COMMENT '任务日志id',
  `job_id` char(19) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='定时任务日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_log`
--

LOCK TABLES `schedule_log` WRITE;
/*!40000 ALTER TABLE `schedule_log` DISABLE KEYS */;
INSERT INTO `schedule_log` VALUES ('1606848029841711105','1560590553932980225','TASK_commonTask','Y',0,NULL,1,'2022-12-25 11:03:19'),('1614958094612193281','1560590553932980225','TASK_commonTask','Y',0,NULL,1,'2023-01-16 20:09:49'),('1614958370039554050','1597922030789869570','TASK_errorTask','N',1,NULL,2,'2023-01-16 20:10:55');
/*!40000 ALTER TABLE `schedule_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_api_log`
--

DROP TABLE IF EXISTS `sys_api_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_api_log` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求用户ID',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求地址',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求方法',
  `query_string` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'url请求参数',
  `form_data` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求表单数据',
  `json_data` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求JSON数据',
  `request_time` datetime DEFAULT NULL COMMENT '请求时间',
  `response_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '返回code',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_api_log_id_IDX` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='API请求日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_api_log`
--

LOCK TABLES `sys_api_log` WRITE;
/*!40000 ALTER TABLE `sys_api_log` DISABLE KEYS */;
INSERT INTO `sys_api_log` VALUES ('1628029157684330498','1526923684416102402','/sa/organ/list','GET','name=',NULL,NULL,'2023-02-21 21:49:31','200'),('1628029172737687553','1526923684416102402','/sa/organ/get','GET','id=1529796266706362369',NULL,NULL,'2023-02-21 21:49:37','200'),('1628029172783824898','1526923684416102402','/sa/organ/parent-list','GET',NULL,NULL,NULL,'2023-02-21 21:49:37','200'),('1628029190727057410','1526923684416102402','/sa/organ/save-or-update','POST',NULL,'id=1529796266706362369&pid=0&name=华南总机构&type=bank&manage=-&managePhone=18726995766&sort=0&status=true&regionId=1529446353599619074&version=0',NULL,'2023-02-21 21:49:41','200'),('1628029197605715969','1526923684416102402','/sa/organ/list','GET','name=',NULL,NULL,'2023-02-21 21:49:43','200'),('1628029203502907393','1526923684416102402','/sa/organ/get','GET','id=1529812318349697026',NULL,NULL,'2023-02-21 21:49:44','200'),('1628029203704233985','1526923684416102402','/sa/organ/parent-list','GET',NULL,NULL,NULL,'2023-02-21 21:49:44','200'),('1628029215465062402','1526923684416102402','/sa/organ/save-or-update','POST',NULL,'id=1529812318349697026&pid=1529796266706362369&name=景云&type=oper&manage=杨峰智&managePhone=18726995766&sort=0&status=true&regionId=1529404229357375490&version=0',NULL,'2023-02-21 21:49:47','200'),('1628029219801972738','1526923684416102402','/sa/organ/list','GET','name=',NULL,NULL,'2023-02-21 21:49:48','200'),('1628029226215063553','1526923684416102402','/sa/organ/get','GET','id=1529827055405916161',NULL,NULL,'2023-02-21 21:49:49','200'),('1628029226424778753','1526923684416102402','/sa/organ/parent-list','GET',NULL,NULL,NULL,'2023-02-21 21:49:50','200'),('1628029239926243329','1526923684416102402','/sa/organ/save-or-update','POST',NULL,'id=1529827055405916161&pid=0&name=测试机构&type=oper&manage=-&managePhone=18726995766&sort=0&status=true&regionId=1529452575958654977&version=0',NULL,'2023-02-21 21:49:53','200'),('1628029243717894146','1526923684416102402','/sa/organ/list','GET','name=',NULL,NULL,'2023-02-21 21:49:54','200'),('1628029250223259650','1526923684416102402','/sa/organ/get','GET','id=1529827122216984577',NULL,NULL,'2023-02-21 21:49:55','200'),('1628029250403614722','1526923684416102402','/sa/organ/parent-list','GET',NULL,NULL,NULL,'2023-02-21 21:49:55','200'),('1628029266073534466','1526923684416102402','/sa/organ/save-or-update','POST',NULL,'id=1529827122216984577&pid=0&name=测试机构2&type=house&manage=-&managePhone=18726995766&sort=0&status=true&regionId=1529404876903387137&version=0',NULL,'2023-02-21 21:49:59','200'),('1628029269915516929','1526923684416102402','/sa/organ/list','GET','name=',NULL,NULL,'2023-02-21 21:50:00','200'),('1628029274910932993','1526923684416102402','/sa/organ/get','GET','id=1604819397195714561',NULL,NULL,'2023-02-21 21:50:01','200'),('1628029275150008321','1526923684416102402','/sa/organ/parent-list','GET',NULL,NULL,NULL,'2023-02-21 21:50:01','200'),('1628029287841972226','1526923684416102402','/sa/organ/save-or-update','POST',NULL,'id=1604819397195714561&pid=1529827122216984577&name=测23&type=bank&manage=右键&managePhone=111&sort=0&status=true&regionId=1529452923024728065&version=1',NULL,'2023-02-21 21:50:04','200'),('1628029291138695170','1526923684416102402','/sa/organ/list','GET','name=',NULL,NULL,'2023-02-21 21:50:05','200'),('1628029304954732545','1526923684416102402','/sa/organ/parent-list','GET',NULL,NULL,NULL,'2023-02-21 21:50:08','200');
/*!40000 ALTER TABLE `sys_api_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_attachment`
--

DROP TABLE IF EXISTS `sys_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_attachment` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `filename` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件名',
  `original_filename` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '原始文件名',
  `file_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件后缀名',
  `file_size` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件大小',
  `absolute_path` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '绝对路径',
  `web_path` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '网络路径',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `table_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关联的表名',
  `table_field` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关联表的字段名',
  `table_pk` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关联表的主键',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `file_src` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'local' COMMENT '文件存储位置',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_attachment_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统附件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_attachment`
--

LOCK TABLES `sys_attachment` WRITE;
/*!40000 ALTER TABLE `sys_attachment` DISABLE KEYS */;
INSERT INTO `sys_attachment` VALUES ('1598712671211900929','15987126712119009293.jpg','3.jpg','jpg','36KB',NULL,'http://qiniuy.wangijun.com/1598712671211900929','2022-12-03 00:16:18','1526923684416102402',NULL,NULL,NULL,'用户上传头像','qiniu'),('1598714674960945154','15987146749609451545.jpg','5.jpg','jpg','50KB',NULL,'http://qiniuy.wangijun.com/1598714674960945154','2022-12-03 00:24:16','1538457626805809153',NULL,NULL,NULL,'用户上传头像','qiniu'),('1598714862010126338','1598714862010126338113.jpg','113.jpg','jpg','78KB',NULL,'http://qiniuy.wangijun.com/1598714862010126338','2022-12-03 00:25:00','1571073304574418946',NULL,NULL,NULL,'用户上传头像','qiniu'),('1598715027785797633','159871502778579763313.gif','13.gif','gif','157KB',NULL,'http://qiniuy.wangijun.com/1598715027785797633','2022-12-03 00:25:40','1571073778639822849',NULL,NULL,NULL,'用户上传头像','qiniu'),('1599301941731651586','159930194173165158611.jpg','11.jpg','jpg','30KB',NULL,'http://qiniuy.wangijun.com/1599301941731651586','2022-12-04 15:17:51','1526923684416102402',NULL,NULL,NULL,'用户上传头像','qiniu'),('1604688947626196994','16046889476261969944.jpg','4.jpg','jpg','37KB',NULL,'http://qiniuy.wangijun.com/1604688947626196994','2022-12-19 12:03:53','1526923684416102402',NULL,NULL,NULL,'通知公告图片','qiniu');
/*!40000 ALTER TABLE `sys_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config` (
  `config_key` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '配置Key',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '配置内容',
  `config_desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '配置描述',
  PRIMARY KEY (`config_key`),
  UNIQUE KEY `sys_config_config_key_uindex` (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_cron`
--

DROP TABLE IF EXISTS `sys_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_cron` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `expression` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'cron表达式',
  `desc` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_cron_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='常用定时任务表达式';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_cron`
--

LOCK TABLES `sys_cron` WRITE;
/*!40000 ALTER TABLE `sys_cron` DISABLE KEYS */;
INSERT INTO `sys_cron` VALUES ('1535579757629935618','0 15 10 ? * *','每天上午10:15执行任务'),('1535579941722132482','0 15 10 * * ?','每天上午10:15执行任务'),('1535579993085579266','0 0 12 * * ?','每天中午12:00执行任务'),('1535580042473508866','0 0 10,14,16 * * ?','每天上午10:00点、下午14:00以及下午16:00执行任务'),('1535580099616706561','0 0/30 9-17 * * ?','每天上午09:00到下午17:00时间段内每隔半小时执行任务'),('1535580154792775681','0 * 14 * * ?','每天下午14:00到下午14:59时间段内每隔1分钟执行任务'),('1535580205279612929','0 0-5 14 * * ?','每天下午14:00到下午14:05时间段内每隔1分钟执行任务'),('1535580260426321921','0 0/5 14 * * ?','每天下午14:00到下午14:55时间段内每隔5分钟执行任务'),('1535580312184033282','0 0/5 14,18 * * ?','每天下午14:00到下午14:55、下午18:00到下午18:55时间段内每隔5分钟执行任务'),('1535580366026313730','0 0 12 ? * WED','每个星期三中午12:00执行任务'),('1535580430811533313','0 15 10 15 * ?','每月15日上午10:15执行任务'),('1535580478530129921','0 15 10 L * ?','每月最后一日上午10:15执行任务'),('1535580587783360514','0 15 10 ? * 6L','每月最后一个星期六上午10:15执行任务'),('1535580672315363329','0 15 10 ? * 6#3','每月第三个星期六上午10:15执行任务'),('1535580728548397057','0 10,44 14 ? 3 WED','每年3月的每个星期三下午14:10和14:44执行任务'),('1535580782919159810','0 15 10 ? * * 2022','2022年每天上午10:15执行任务'),('1535580840825720833','0 15 10 ? * * *','每年每天上午10:15执行任务'),('1535580899231404033','0 0/5 14,18 * * ? 20','2022年每天下午14:00到下午14:55、下午18:00到下午18:55时间段内每隔5分钟执行任务'),('1535580945213558786','0 15 10 ? * 6#3 2022','2022年至2023年每月第三个星期六上午10:15执行任务'),('1535581020534870018','0 0/30 9-17 * * ? 20','2022年至2025年每天上午09:00到下午17:30时间段内每隔半小时执行任务'),('1535581074310041602','0 10,44 14 ? 3 WED 2','从2022年开始，每隔两年3月的每个星期三下午14:10和14:44执行任务'),('1535583844140322818','*/5 * * * * ?','每五秒执行一次'),('1535583941230071809','0 * * * * ?','每分钟触发一次'),('1535584008271826945','0 0 * * * ?','每小时触发一次'),('1535584070876008450','0 0 1 * * ?','每天凌晨1点触发一次'),('1535584136294567937','0 30 1,12 * * ?','每天凌晨1:30和12:30各触发一次'),('1535584191776821250','0 * 1,13 * * ?','每天1:00到1:59以及13::00到13::59，每分钟触发一次'),('1535584308026150914','0 * 1 * * ?','每天凌晨1:00到1:59，每分钟触发一次');
/*!40000 ALTER TABLE `sys_cron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dept` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `pid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '父级Id',
  `name` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `manage` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理人',
  `manage_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理人联系电话',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，true启用；false禁用',
  `create_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_dept_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统部门表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES ('1530856961204031490','0','景云总公司','-','-','没啥好说的呀',0,1,NULL,NULL,NULL,NULL,0),('1530857401958273026','1530856961204031490','开发部1','-','-',NULL,0,1,NULL,NULL,'1526923684416102402','2023-01-17 11:10:32',1);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict`
--

DROP TABLE IF EXISTS `sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典名称',
  `code` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典编码',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT '0' COMMENT '排序',
  `create_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_dict_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统字典表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict`
--

LOCK TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` VALUES ('1627605047158288386','房屋类型','house_type','????',0,'1526923684416102402','2023-02-20 17:44:17','1526923684416102402','2023-02-21 21:06:06',3),('1627913740924022785','机构类型','organ_type','??',0,'1526923684416102402','2023-02-21 14:10:56','1526923684416102402','2023-02-21 21:06:31',1);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_info`
--

DROP TABLE IF EXISTS `sys_dict_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_info` (
  `id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '注解',
  `dict_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典编码',
  `value` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '选项的值',
  `label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '选项的标签',
  `disabled` tinyint(1) DEFAULT '0' COMMENT '是否禁用',
  `sort` int DEFAULT '0' COMMENT '排序',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字典数据项';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_info`
--

LOCK TABLES `sys_dict_info` WRITE;
/*!40000 ALTER TABLE `sys_dict_info` DISABLE KEYS */;
INSERT INTO `sys_dict_info` VALUES ('1628022347288023041','house_type','1','首套房',0,0,NULL,NULL,NULL,NULL,3),('1628025271422554114','house_type','2','二套房',0,1,NULL,NULL,NULL,NULL,0),('1628027518864859137','organ_type','bank','银行',0,0,NULL,NULL,NULL,NULL,0),('1628027569024540673','organ_type','house','房管局',0,1,NULL,NULL,NULL,NULL,1),('1628027637131649025','organ_type','oper','开发商',0,2,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_dict_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_generator_config`
--

DROP TABLE IF EXISTS `sys_generator_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_generator_config` (
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户ID，主键',
  `generator_config` json DEFAULT NULL COMMENT '代码生成配置',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='代码生成记忆配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_generator_config`
--

LOCK TABLES `sys_generator_config` WRITE;
/*!40000 ALTER TABLE `sys_generator_config` DISABLE KEYS */;
INSERT INTO `sys_generator_config` VALUES ('1571073304574418946','{\"idType\": \"ASSIGN_ID\", \"xmlName\": \"com.wlf.server.mapper.xml\", \"tempList\": [\"ENTITY\", \"SERVICE_IMPL\", \"CONTROLLER\", \"MAPPER\", \"XML\", \"VUE\"], \"authorName\": \"清欢\", \"entityName\": \"com.wlf.server.entity\", \"mapperName\": \"com.wlf.server.mapper\", \"serviceName\": \"com.wlf.server.service\", \"enableLombok\": true, \"enableSwagger\": false, \"addTablePrefix\": \"t_\", \"controllerName\": \"com.wlf.server.controller\", \"serviceFileName\": \"ServiceInterface\", \"serviceImplName\": \"com.wlf.server.service\", \"enableCapitalMode\": false, \"serviceImplFilename\": \"Service\", \"enableMapperAnnotation\": true}');
/*!40000 ALTER TABLE `sys_generator_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `pid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '父级ID',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单图标',
  `info` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单介绍',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单指向地址',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示',
  `is_blank` tinyint(1) DEFAULT '0' COMMENT '是否外部链接',
  `type` int DEFAULT '1' COMMENT '菜单类别，1菜单和2按钮，如果存在子菜单则自动变为目录。',
  `menu_level` int DEFAULT '0' COMMENT '菜单级别,最多支持4级菜单也就是说，这个值不能大于4',
  `permission` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '权限标识',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者ID',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_menu_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES ('1523286993952337922','0','系统管理','el-icon-setting','系统管理演示','-----',1,0,1,0,'-----',0,NULL,NULL,'1526923684416102402','2023-02-21 14:12:10',1),('1523563884572651522','1523286993952337922','菜单管理','el-icon-menu',NULL,'module/sys/menu.vue',1,0,1,1,'module:sys:menu',0,NULL,NULL,NULL,NULL,0),('1524005027223457794','1523286993952337922','角色管理','el-icon-s-custom','????','module/sys/role.vue',1,0,1,1,'module:sys:role',1,NULL,NULL,NULL,NULL,0),('1526072650152017922','1523286993952337922','用户管理','el-icon-user-solid','','module/sys/user.vue',1,0,1,1,'module:sys:user',2,NULL,NULL,NULL,NULL,0),('1527292472508583937','1524005027223457794','主列表','','','/role/list',1,0,2,2,'role:list',0,NULL,NULL,'1526923684416102402','2023-02-16 09:46:48',1),('1527292604293615618','1524005027223457794','详情','','','/role/get',1,0,2,2,'role:get',1,NULL,NULL,NULL,NULL,0),('1527542136449269761','1524005027223457794','新增或修改','','没备注','/role/save-or-update',1,0,2,2,'role:save-or-update',3,NULL,NULL,NULL,NULL,0),('1528560593489068034','1524005027223457794','删除','','','/role/del',1,0,2,2,'role:del',4,NULL,NULL,'1526923684416102402','2023-02-21 14:12:55',1),('1528672788398927873','1523286993952337922','附件管理','el-icon-files',NULL,'module/sys/attachment.vue',1,0,1,1,'module:sys:attachment',3,NULL,NULL,NULL,NULL,0),('1529095409938599937','1536178843588546562','机构管理','el-icon-office-building',NULL,'module/sys/organ.vue',1,0,1,1,'module:sys:organ',5,NULL,NULL,NULL,NULL,0),('1529095970024984577','1536178843588546562','部门管理','el-icon-postcard',NULL,'module/sys/dept.vue',1,0,1,1,'module:sys:dept',6,NULL,NULL,NULL,NULL,0),('1529096285323399169','1536178843588546562','岗位管理','el-icon-trophy-1',NULL,'module/sys/post.vue',1,0,1,1,'module:sys:post',7,NULL,NULL,NULL,NULL,0),('1529096358409146370','1536178843588546562','区域管理','el-icon-position',NULL,'module/sys/region.vue',1,0,1,1,'module:sys:region',1,NULL,NULL,NULL,NULL,0),('1530870873920126977','1523286993952337922','字典管理','el-icon-reading',NULL,'module/sys/dict.vue',1,0,1,1,'module:sys:dict',4,NULL,NULL,NULL,NULL,0),('1531943262054514689','0','定时任务','el-icon-timer',NULL,'-',1,0,1,0,'-',2,NULL,NULL,NULL,NULL,0),('1531943903069995010','1531943262054514689','任务管理','el-icon-place',NULL,'module/sys/task.vue',1,0,1,1,'module:sys:task',0,NULL,NULL,NULL,NULL,0),('1535469167083343874','1531943262054514689','常用Cron表达式','el-icon-s-operation',NULL,'module/sys/cron.vue',1,0,1,1,'module:sys:cron',2,NULL,NULL,NULL,NULL,0),('1536178843588546562','0','组织架构','el-icon-office-building',NULL,'-',1,0,1,0,'-',1,NULL,NULL,NULL,NULL,0),('1563712125166198786','0','系统公告','el-icon-message',NULL,'-',1,0,1,0,'-',4,NULL,NULL,NULL,NULL,0),('1564258642717343746','1563712125166198786','公告管理',NULL,NULL,'module/notice/list.vue',1,0,1,1,'module:notice:list',1,NULL,NULL,NULL,NULL,0),('1565955611370237954','1523286993952337922','代码生成','el-icon-toilet-paper',NULL,'module/generator/list.vue',1,0,1,1,'module:generator:list',5,NULL,NULL,NULL,NULL,0),('1567147402169778177','1523286993952337922','API请求日志','el-icon-sugar',NULL,'module/sys/api-log.vue',1,0,1,1,'module:sys:api-log',6,NULL,NULL,'1526923684416102402','2023-02-21 14:12:46',2),('1567889503241162754','1523286993952337922','在线聊天室','el-icon-phone-outline',NULL,'module/sys/im.vue',1,0,1,1,'module:sys:im',7,NULL,NULL,NULL,NULL,0),('1606605956072914945','1523286993952337922','系统监控','el-icon-odometer',NULL,'module/sys/actuator.vue',1,0,1,1,'module:sys:actuator',8,NULL,NULL,NULL,NULL,0),('1606670446193860610','1523563884572651522','主列表','',NULL,'/menu/list',1,0,2,2,'menu:list',0,NULL,NULL,NULL,NULL,0),('1606670519074086913','1523563884572651522','详情',NULL,NULL,'/menu/get',1,0,2,2,'menu:get',0,NULL,NULL,NULL,NULL,0),('1606670637227630593','1523563884572651522','新增或修改',NULL,NULL,'/menu/save-or-update',1,0,2,2,'menu:save-or-update',0,NULL,NULL,'1526923684416102402','2023-02-16 09:46:37',1),('1606670759877468161','1523563884572651522','删除',NULL,NULL,'/menu/del',1,0,2,2,'menu:del',0,NULL,NULL,NULL,NULL,0),('1606675177129717762','1526072650152017922','主列表',NULL,NULL,'/user/list',1,0,2,2,'user:list',0,NULL,NULL,NULL,NULL,0),('1606675494235877377','1526072650152017922','查看',NULL,NULL,'/user/get',1,0,2,2,'user:get',0,NULL,NULL,NULL,NULL,0),('1606680007504113666','1528672788398927873','主列表',NULL,NULL,'/attachment/list',1,0,2,2,'attachment:list',0,NULL,NULL,NULL,NULL,0),('1606680121350107137','1530870873920126977','主列表',NULL,NULL,'/dict/list',1,0,2,2,'dict:list',0,NULL,NULL,NULL,NULL,0),('1606680270298230786','1565955611370237954','主列表',NULL,NULL,'/generator/list',1,0,2,2,'generator:list',0,NULL,NULL,NULL,NULL,0),('1606680437172809729','1567147402169778177','主列表',NULL,NULL,'/sys:api-log/list',1,0,2,2,'sys:api-log:list',0,NULL,NULL,NULL,NULL,0),('1606681209289650178','1529096358409146370','主列表',NULL,NULL,'/region/list',1,0,2,2,'region:list',0,NULL,NULL,NULL,NULL,0),('1606681300448653313','1529095409938599937','主列表',NULL,NULL,'/organ/list',1,0,2,2,'organ:list',0,NULL,NULL,NULL,NULL,0),('1606681372733288449','1529095970024984577','主列表',NULL,NULL,'/dept/list',1,0,2,2,'dept:list',0,NULL,NULL,NULL,NULL,0),('1606681453922430978','1529096285323399169','主列表',NULL,NULL,'/post/list',1,0,2,2,'post:list',0,NULL,NULL,NULL,NULL,0),('1606692704559828993','1531943903069995010','主列表',NULL,NULL,'/task/list',1,0,2,2,'task:list',0,NULL,NULL,NULL,NULL,0),('1606693094537826305','1531943903069995010','修改任务状态',NULL,NULL,'/task/update-status',1,0,2,2,'task:update-status',0,NULL,NULL,NULL,NULL,0),('1606702769446637569','task-log','主列表',NULL,NULL,'/task/log/list',1,0,2,2,'task:log:list',0,NULL,NULL,NULL,NULL,0),('1606703054365708290','task-log','删除日志',NULL,NULL,'/task/log/del',1,0,2,2,'task:log:del',0,NULL,NULL,NULL,NULL,0),('1606704387885944834','1535469167083343874','主列表',NULL,NULL,'/task/cron/list',1,0,2,2,'task:cron:list',0,NULL,NULL,NULL,NULL,0),('1612074880809893890','1523563884572651522','导出菜单',NULL,'高危权限！！非开发者请勿赋予，主要用来同步生产和开发上的菜单数据，免得需要手动或者生成SQL去添加，','/menu/export',1,0,2,2,'menu:export',0,NULL,NULL,'1526923684416102402','2023-01-19 14:22:18',1),('1612074984715386881','1523563884572651522','导入菜单',NULL,'高危权限！非开发者请勿赋予，就是来说就是在开发配置好菜单的情况下，导出一份文件，然后到生产上导入，即可。','/menu/import-menu',1,0,2,2,'menu:import-menu',0,NULL,NULL,'1526923684416102402','2023-01-19 14:23:23',2),('notice-add','1563712125166198786','编辑公告',NULL,NULL,'module/notice/editor.vue',1,0,1,1,'module:notice:editor',0,NULL,NULL,NULL,NULL,0),('task-log','1531943262054514689','任务日志','el-icon-lollipop',NULL,'module/sys/task-log.vue',1,0,1,1,'module:sys:task-log',1,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_notice`
--

DROP TABLE IF EXISTS `sys_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_notice` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公告标题',
  `content` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公告内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建者',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公告状态， none("未发布"),todo("草稿"),  release("已发布")',
  `update_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统公告表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_notice`
--

LOCK TABLES `sys_notice` WRITE;
/*!40000 ALTER TABLE `sys_notice` DISABLE KEYS */;
INSERT INTO `sys_notice` VALUES ('1567864102062198785','第11111次11','<h1 style=\"text-align: center;\">你好世界</h1><p style=\"text-align: center;\"><img src=\"http://qiniuy.wangijun.com/1604688947626196994\" alt=\"4.jpg\" data-href=\"\" style=\"width: 30%;\"></p><p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\">你会等待还是离开233</p>','2023-01-17 11:15:42','1526923684416102402','todo','1526923684416102402','2023-01-17 11:15:42',1),('1600106270579245057','默认标题','<h1 style=\"text-align: center;\">你好世界</h1><p>这是公告内容233333333333333333333333333</p>','2022-12-06 20:33:58','1526923684416102402','release',NULL,NULL,0),('1604821786757812226','试一试长标题什么样子的123123123','<h1 style=\"text-align: center;\">你好世界</h1><p>这是公告内容</p>','2022-12-19 20:51:45','1526923684416102402','release',NULL,NULL,0);
/*!40000 ALTER TABLE `sys_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_organ`
--

DROP TABLE IF EXISTS `sys_organ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_organ` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `pid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '父级机构Id',
  `name` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '机构名称',
  `type` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '机构类型',
  `manage` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '机构管理人',
  `manage_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '机构管理人联系电话',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，true启用；false禁用',
  `region_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '所属区域ID',
  `create_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_organ_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统机构表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_organ`
--

LOCK TABLES `sys_organ` WRITE;
/*!40000 ALTER TABLE `sys_organ` DISABLE KEYS */;
INSERT INTO `sys_organ` VALUES ('1529796266706362369','0','华南总机构','bank','-','18726995766',NULL,0,1,'1529446353599619074',NULL,NULL,'1526923684416102402','2023-02-21 21:49:41',1),('1529812318349697026','1529796266706362369','景云','oper','杨峰智','18726995766',NULL,0,1,'1529404229357375490',NULL,NULL,'1526923684416102402','2023-02-21 21:49:47',1),('1529827055405916161','0','测试机构','oper','-','18726995766',NULL,0,1,'1529452575958654977',NULL,NULL,'1526923684416102402','2023-02-21 21:49:53',1),('1529827122216984577','0','测试机构2','house','-','18726995766',NULL,0,1,'1529404876903387137',NULL,NULL,'1526923684416102402','2023-02-21 21:49:59',1),('1604819397195714561','1529827122216984577','测23','bank','右键','111',NULL,0,1,'1529452923024728065',NULL,NULL,'1526923684416102402','2023-02-21 21:50:04',2);
/*!40000 ALTER TABLE `sys_organ` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_post`
--

DROP TABLE IF EXISTS `sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_post` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `pid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '父级Id',
  `name` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `manage` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理人',
  `manage_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理人联系电话',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，true启用；false禁用',
  `create_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_post_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统岗位表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_post`
--

LOCK TABLES `sys_post` WRITE;
/*!40000 ALTER TABLE `sys_post` DISABLE KEYS */;
INSERT INTO `sys_post` VALUES ('1530866900618199041','0','总经理','-','-',NULL,0,1,NULL,NULL,NULL,NULL,0),('1530867099335933953','1530866900618199041','人事1','-','-',NULL,0,1,NULL,NULL,'1526923684416102402','2023-01-17 11:10:55',1);
/*!40000 ALTER TABLE `sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_region`
--

DROP TABLE IF EXISTS `sys_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_region` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `pid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '父级Id',
  `name` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `manage` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理人',
  `manage_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理人联系电话',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `sort` int DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，true启用；false禁用',
  `create_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_region_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统区域表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_region`
--

LOCK TABLES `sys_region` WRITE;
/*!40000 ALTER TABLE `sys_region` DISABLE KEYS */;
INSERT INTO `sys_region` VALUES ('1529404229357375490','0','华北地区','-','-','华东华北一家人',0,1,NULL,NULL,NULL,NULL,0),('1529404808943079425','1529404229357375490','东北省','-','-',NULL,0,1,NULL,NULL,NULL,NULL,0),('1529404876903387137','1529404808943079425','葫芦岛','-','-',NULL,0,1,NULL,NULL,NULL,NULL,0),('1529446353599619074','0','华南地区','-','-',NULL,0,1,NULL,NULL,NULL,NULL,0),('1529452529460600834','1529446353599619074','杭州市','-','-',NULL,0,1,NULL,NULL,NULL,NULL,0),('1529452575958654977','1529452529460600834','静安区1','-','-',NULL,0,1,NULL,NULL,'1526923684416102402','2023-01-17 11:09:28',1),('1529452923024728065','1529452529460600834','测试区','-','-',NULL,0,1,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `pid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '父ID（暂时不用）',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '角色名',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '角色编码',
  `info` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：true启用；false禁用',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int DEFAULT '0' COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_role_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES ('1524721679870304257','','超级管理','admin','root角色',1,NULL,NULL,NULL,NULL,0),('1526547919887429634','','操作员','oper','',1,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_serial`
--

DROP TABLE IF EXISTS `sys_serial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_serial` (
  `serial_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_no` int DEFAULT NULL COMMENT '序列号',
  `generate_date` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '生成时间',
  PRIMARY KEY (`serial_type`),
  UNIQUE KEY `sys_serial_serial_type_uindex` (`serial_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='自定义序列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_serial`
--

LOCK TABLES `sys_serial` WRITE;
/*!40000 ALTER TABLE `sys_serial` DISABLE KEYS */;
INSERT INTO `sys_serial` VALUES ('LOGIN_NO',8,'');
/*!40000 ALTER TABLE `sys_serial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `avatar` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户头像',
  `login_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录账号',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码加密用的随机字段',
  `password` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录密码',
  `sex` int DEFAULT NULL COMMENT '性别，0男，1女，2未知',
  `user_type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户类别，0系统用户，1普通用户',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：true启用；false禁用',
  `login_end_time` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `version` int DEFAULT '0' COMMENT '版本号，乐观锁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_user_id_uindex` (`id`),
  UNIQUE KEY `sys_user_login_no_uindex` (`login_no`),
  UNIQUE KEY `sys_user_phone_uindex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES ('1526923684416102402','无良方','http://qiniuy.wangijun.com/1599301941731651586','001','15678431234',NULL,'000000',0,'SYS',1,'2023-02-21 12:18:46','1526923684416102402','2023-02-21 12:18:46',NULL,'2022-05-18 21:52:30','没啥好说的2',1),('1538457626805809153','有2','http://qiniuy.wangijun.com/1598714674960945154','NORM0006','18726887655',NULL,'000000',0,'NORM',1,'2022-12-06 20:14:46','1526923684416102402','2023-01-17 09:50:53',NULL,'2022-06-19 17:44:16','23333',1),('1571073304574418946','有三','http://qiniuy.wangijun.com/1598714862010126338','SYS0007','13111112222',NULL,'000000',1,'SYS',1,'2022-12-03 00:24:42',NULL,NULL,NULL,'2022-09-17 17:47:20','就是一个正常滴用户',0),('1571073778639822849','有四','http://qiniuy.wangijun.com/1598715027785797633','BANK0008','13122221111',NULL,'000000',1,'BANK',1,'2022-12-03 00:25:20','1526923684416102402','2023-01-19 14:13:31',NULL,'2022-09-17 17:49:13','44444444',1);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'sa_server'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-21 21:50:31
