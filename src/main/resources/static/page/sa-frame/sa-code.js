// 在使用时，不建议你直接魔改模板的代码，以免在运行时出现意外bug，而是在本文件中根据模板的提供的API，来适应你的业务逻辑
// ....


// ================================= 示例：一些基本信息 =================================

// 设置模板标题
sa_admin.title = "w_admin";
sa_admin.logo = 'sa-frame/logo.jpg';    // 设置logo图标地址
sa_admin.icon = 'sa-frame/logo.jpg';    // 设置icon图标地址

sa_admin.model = "2"// 模式 1，离线模式，2在线模式，

// 菜单加载，1只加载后台菜单，2，后台菜单+演示菜单
sa_admin.menuType = "1"


// ================================= 示例：自定义菜单 =================================

//var myMenuList = window.menuList;		// window.menuList 在 menu-list.plugins 中定义
//sa_admin.setMenuList(myMenuList);	// 写入菜单
// sa_admin.setMenuList(myMenuList, [11, 1, '1-1']);	// 写入菜单，并设置应该显示哪些id的菜单（第二个参数为空时，代表默认显示所有）
// sa_admin.initMenu();	// 更简单的写法，相当于setMenuList省略第一个参数

// 如果需要获得更多操作能力，如：动态添加菜单、删除菜单等
// 可直接 sa_admin.menuList 获得菜单引用，直接操作对象


// ================================= 示例：js控制打开某个菜单 =================================

// 显示主页选项卡
// sa_admin.showHome();

// 显示一个选项卡, 根据id
// sa_admin.showTabById('1-1');

// 关闭一个选项卡，根据 id
// sa_admin.closeTabById('1-1');

// 新增一个选项卡
// sa_admin.addTab({id: 12345, name: '新页面', url: 'http://web.yanzhi21.com'});	// id不要和已有的菜单id冲突，其它属性均可参照菜单项

// 新增一个选项卡、并立即显示
// sa_admin.showTab({id: 12345, name: '新页面', url: 'http://web.yanzhi21.com'});	// 参数同上

// 打开一个 菜单，根据 id
// sa_admin.showMenuById('1-1');

// ================================= 示例：设置user信息 =================================
// 用户登录后，右上角可直接显示用户的头像和昵称
sa_admin.user = {
    username: 'root',	// 昵称
    avatar: 'sa-frame/admin-logo.png'	// 头像地址
}


// ================================= 示例：设置登录后的头像处，下拉可以出现的选项  =================================
sa_admin.dropList = [		// 头像点击处可操作的选项
    {
        name: '我的资料',
        click: function () {
            let ids = sa_admin.tabList.map(i => i.id).toString();
            if (ids.indexOf('personal') === -1) {
                sa_admin.showTab({id: 'personal', name: '个人主页', url: 'module/sys/personal.vue'});
            } else {
                sa_admin.showTabById('personal');
            }

        }
    },
    {
        name: '切换账号',
        click: function () {
            sa.$page.openLogin('login.html');
            // layer.open({
            // 	type: 2,
            // 	title: false,
            // 	closeBtn: false,
            // 	shadeClose: true,
            // 	shade: 0.8,
            // 	area: ['70%', '80%'],
            // 	resize: false,
            // 	content: 'login.html'
            // });
        }
    },
    {
        name: '退出登录',
        click: function () {
            // sa_admin.$message('点击了退出登录，你可以参照文档重写此函数');
            // location="login.html";
            sa.confirm('退出登录？', function (res) {
                sa.ajax("/main/logout", {}, (res) => {
                    sa.alert(res.msg, () => {
                        // 关闭长连接
                        sa.webSocket.close()
                        // 跳转到登录页。
                        window.open("login.html", '_self')
                    })
                }, {type: 'get', msg: '正在注销'})
            });
        }
    },
    {
        name: '清理缓存',
        click: function () {
            sa.confirm('确认清理Redis中的缓存？', () => {
                sa.ajax('/main/clean-cache',{},(res)=>{
                    sa.ok(res.msg)
                },{type: 'get'})
            })
        }
    },
    {
        name: '刷新菜单',
        click: function () {
            sa.confirm('刷新菜单会丢失当前页面表单中的数据，确认刷新？', () => {
                sa.f5()
            })
        }
    }
]

// ================================= 示例：调用另一个页面的代码 =================================
// var win = sa_admin.getTabWindow('2-1');		// 根据id获取其页面的window对象   （如果此页面未打开，则返回空）（跨域模式下无法获取其window对象）
// win.app.f5();

// 注意:
// 根据`iframe`的子父通信原则，在子页面中调用父页面的方法，需要加上parent前缀，例如：
// parent.sa_admin.msg('啦啦啦');		// 调用父页面的弹窗方法

sa.wsCallback = new Map()
// 就是依据这个Key来寻找对应的回调函数。
sa.wsCallback.set('notice', (data, ws) => {
    layer.confirm('您有新的公告！！',{
        btn: ['点击查看','稍后在看'],
        yes: function (index) {
            layer.close(index)
            layer.open({
                title: data.title,
                type: 1,
                content: data.content,//这里content是一个普通的String
                shadeClose: false,	// 是否点击遮罩关闭
                maxmin: true, // 显示最大化按钮
                shade: 0.8,		// 遮罩透明度
                scrollbar: false,	// 屏蔽掉外层的滚动条
                moveOut: true,		// 是否可拖动到外面
                area: ['60%', '80%'],	// 大小
                btn: ['确认收到'],
                yes: function (index,layero) {
                    console.log(data)
                    sa.ajax('/notice/reading', {id: data.id}, (res) => {
                        sa.alert(res.msg, () => {
                        })
                    }, {type: 'get', msg: null})
                }
            });
        },
        btn2: function (index){
            layer.close(index)
        }
    })



})


function initWs(userId) {
    // 写入菜单 这里应该有两种模式，第一种演示模式，会带上原有项目中的样例
    sa.webSocket = new sa.createWS("ws", sa.cfg.ws_ip, sa.cfg.ws_port, userId,
        new function () {
            this.onopen = function (ev, ws) {
                sa.msgOk('WebSocket已连接')
            }
            this.onmessage = function (ev, ws) {
                let data = JSON.parse(ev.data);
                /* 注意这里，服务器传来的数据应该是这个格式
                {
                code: '', // 这个值决定了调用哪个回调函数。
                data: xxxx, 这个值会传给回调函数
                }
                * */
                let callback = sa.wsCallback.get(data.code);
                if (callback){
                    return callback(data.data, ws);
                } else {
                    return sa.msgError('接收到服务器推送过来的消息，但未找到对应的回调函数！')
                }

            }
            this.onclose = function (ev, ws) {
                sa.msgInfo('WebSocket已关闭')
            }
            this.onerror = function (ev, ws) {
                sa.error('WebSocket错误')
            }
            this.ping = function (ws) {
                ws.send('ping...')
            }
        }, 10000, 3000, "blob")
}

if (sa_admin.model === "1") {
    sa_admin.setMenuList(window.menuList)
    sa_admin.init();
} else {
    // 在线模式
    sa.ajax("/main/init", {}, (res) => {
        sa_admin.user = res.data.userDTO
        initWs(sa_admin.user.id);
        sa_admin.user.username = sa_admin.user.name // 昵称
        sa_admin.user.avatar = sa.getImgPath(sa_admin.user.avatar) // 头像
        sa.$sys.setAuth(sa_admin.user.menuList.map(i => i.permission))
        if (sa_admin.menuType === "1") {
            window.menuList = res.data.menuList;
        } else {
            window.menuList.push(...res.data.menuList)
        }
        sa_admin.setMenuList(window.menuList)
        sa_admin.init();
    }, {
        type: 'get', msg: null, complete: function () {
            // 创建Socket连接
            setTimeout(() => {
                console.log('初始化WebSocket')
                sa.webSocket.connect()
            }, 0)

        }
    })
}


// ================================= 示例：初始化模板(必须调用)  =================================
// 初始化模板
// sa_admin.setMenuList(window.menuList)
// sa_admin.init();


