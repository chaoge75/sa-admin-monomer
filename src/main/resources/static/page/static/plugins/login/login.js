// 登录界面的主JS
$('body').append(
    '<div id="panel_slide_id" style="display: none"></div>\n' +
    '<div id="panel_points_id" style="display: none"></div>\n' +
    '<div id="panel_slide" style="margin-top:50px;"></div>\n' +
    '<div id="panel_points" style="margin-top:50px;"></div>');

$('.forgotBtn').on('click', function () {
    $('#forgot').toggleClass('toggle')
})
$('.registerBtn').on('click', function () {
    $('#register, #formContainer').toggleClass('toggle')
})
let loginName = $('#loginName')
let loginPass = $('#loginPass')

function inputCheck() {
    if (loginName.val().trim() === '') {
        sa.error('用户名不能为空')
        loginName.focus()
        return false
    }
    if (loginPass.val().trim() === '') {
        sa.error('密码不能为空')
        loginPass.focus()
        return false
    }
    return true
}

let PATH = '/main'

function login(params) {
    sa.ajax(PATH + '/login', {}, (res1) => {
        let p = {key: loginName.val(), passwd: aesEncode(loginPass.val(), res1.data)}
        sa.ajax(PATH + '/login', {
            ...p, ...params
        }, function (res) {
            // 登录获取并写入token
            localStorage.setItem('token', JSON.stringify(res.data.token))
            // 跳转页面
            sa.alert(res.msg, () => {
                if (parent === window) {
                    window.open("index.html", '_self')
                } else {
                    sa.closeCurrIframe();
                    parent.location.reload();
                }
            })
        }, {msg: '正在登录.....', sleep: 300})
    }, {msg: null, type: 'get'})
}

// // 初始化验证码  弹出式
$('#panel_slide').slideVerify({
    baseUrl: sa.cfg.api_url,
    mode: 'pop',     //展示模式
    containerId: 'panel_slide_id',//pop模式 必填 被点击之后出现行为验证码的元素id
    imgSize: {       //图片的大小对象,有默认值{ width: '310px',height: '155px'},可省略
        width: '400px',
        height: '200px',
    },
    barSize: {          //下方滑块的大小对象,有默认值{ width: '310px',height: '50px'},可省略
        width: '400px',
        height: '40px',
    },
    beforeCheck: function () {  //检验参数合法性的函数  mode ="pop"有效
        return inputCheck()
    },
    ready: function () {
    },  //加载完毕的回调
    success: login,
    error: function () {
    }        //失败的回调
});
// // 初始化验证码  弹出式
$('#panel_points').pointsVerify({
    baseUrl: sa.cfg.api_url,
    containerId: 'panel_points_id', // pop模式 必填 被点击之后出现行为验证码的元素id
    mode: 'pop',
    imgSize: {       //图片的大小对象
        width: '400px',
        height: '200px',
    },
    beforeCheck: function () {  //检验参数合法性的函数  mode ="pop"有效
        return inputCheck()
    },
    ready: function () {
    },
    success: login,
    error: function () {
    }
});
$('#subLogin').on('click', function () {
    let array = ['panel_slide_id', 'panel_points_id'];
    let value = array[Math.round(Math.random() * (array.length - 1))];  //随机抽取一个值
    $('#' + value).trigger('click')
})

function getAvatar(){
    sa.ajax(PATH + '/avatar', {key: loginName.val()}, (res) => {
        if (res.data){
            $('#avatar').attr('src',sa.getImgPath(res.data))
        } else {
            sa.msgWarn('加载头像失败，使用默认头像')
            $('#avatar').attr('src','static/plugins/login/images/avatar.png')
        }

    }, {type: 'get', msg: null})
}
loginName.on('change', () => {
    if (loginName.val()) {
        getAvatar();
    }
})
// 初始默认用户名以及密码
loginName.val('001')
loginPass.val('000000')
// 加载头像
getAvatar();
