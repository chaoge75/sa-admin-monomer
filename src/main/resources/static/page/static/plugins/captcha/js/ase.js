// 加密数据函数 工具crypto.plugins 文件工具
/**
 * @word 要加密的内容
 * @keyWord String  服务器随机返回的关键字
 *  */
function aesEncrypt(word,keyWord){
  // var keyWord = keyWord || "XwKsGlMcdPMEhR1B"
  var key = CryptoJS.enc.Utf8.parse(keyWord);
  var srcs = CryptoJS.enc.Utf8.parse(word);
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
  return encrypted.toString();
}



// 前端Aes加密，防君子，不防小人。图个乐。
/*加密*/
function aesEncode(str, key) {
  key = CryptoJS.enc.Utf8.parse(key);
  var encryptedData = CryptoJS.AES.encrypt(str, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  return encryptedData.toString();
}


/*解密*/
function aesDecode(encryptedStr, key) {
  key = CryptoJS.enc.Utf8.parse(key);
  var encryptedHexStr = CryptoJS.enc.Base64.parse(encryptedStr);
  var encryptedBase64Str = CryptoJS.enc.Base64.stringify(encryptedHexStr);
  var decryptedData = CryptoJS.AES.decrypt(encryptedBase64Str, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  return decryptedData.toString(CryptoJS.enc.Utf8);
}
