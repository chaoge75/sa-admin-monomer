package ${package.Controller};
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
<#if restControllerStyle>

<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>
 /**
 * 列表数据
 */
 @GetMapping("/list")
 AjaxBean list(String name, ParamPage paramPage){
 return null;
 }

 /**
 * 根据主键获取数据
 */
 @GetMapping("/get")
 AjaxBean get(String id){
 return null;
 }

 /**
 * 修改或更新
 */
 @PostMapping("/save-or-update")
 AjaxBean saveOrUpdate(){
 return null;
 }

 /**
 * 删除数据
 */
 @PostMapping("/del")
 AjaxBean del(String id,String ids){
 return null;
 }

 /**
 * 数据导出
 */
 @GetMapping("/export")
 void export(){

 }

 /**
 * 数据导入
 */
 @PostMapping("/import")
 AjaxBean importData(MultipartFile file){

 return null;
 }

 /**
 * 导入模板下载
 */
 @GetMapping("/temp-down")
 void tempDown(){

 }

 /**
 * 表单初始化
 */
 @GetMapping("/init-form")
 AjaxBean initForm(){
 return null;
 }
}
</#if>
