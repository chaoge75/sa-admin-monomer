package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
    * 系统公告
    */
@Data
public class NoticeDTO implements Serializable {
    /**
     * 主键
     */
    private String id;


    /**
     * 标题
     */
    private String title;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    private String createBy;


    /**
     * 创建者头像
     */
    private String avatar;

    /**
     * 创建者姓名
     */
    private String createByName;

    /**
     * 公告状态，todo草稿，undo已撤回，none默认
     */
    private String type;


    /**
     * 用户读取状态
     */
    private String readStatus;

    /**
     * 版本号
     */
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}