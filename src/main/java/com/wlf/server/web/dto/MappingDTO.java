package com.wlf.server.web.dto;

import com.wlf.server.common.entity.*;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

/**
 * DTO转换，属性名称一致可自动转换，
 * 不一致可使用这种方式来对应
 *  *@Mappings({
 *             *@Mapping(source = "resId", target = "id"),
 *     })
 */

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MappingDTO {
    SysMenu to(MenuDTO menuDTO);

    SysRole to(RoleDTO roleDTO);

    SysDept to(DeptDTO deptDTO);

    SysPost to(PostDTO postDTO);

    List<SysRole> to(List<RoleDTO> roleDTO);

    SysUser to(UserDTO userDTO);
    SysRegion to(RegionDTO regionDTO);

    SysDict to(DictDTO dto);

    SysOrgan to(OrganDTO organDTO);

    OrganDTO toDTO(SysOrgan organ);

    SysDictInfo to(DictInfoDTO dto);
    DictInfoDTO toDTO(SysDictInfo dto);

    RegionDTO toDTO(SysRegion region);


    List<OrganDTO> toOrganDTO(List<SysOrgan> organ);

    List<DeptDTO> toDeptDTO(List<SysDept> dept);

    List<PostDTO> toPostDTO(List<SysPost> post);


    List<MenuDTO> toMenuDTO(List<SysMenu> menus);


    UserDTO toDTO(SysUser sysUser);

    RoleDTO toDTO(SysRole sysRole);

    List<RoleDTO> toRoleDTO(List<SysRole> sysRole);

    SysNotice to(NoticeDTO dto);

    NoticeDTO toDTO(SysNotice sysNotice);

    List<NoticeDTO> toNoticeDTO(List<SysNotice> sysNotices);
}
