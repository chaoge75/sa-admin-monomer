package com.wlf.server.web.dto;


import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
    * 系统字典表
    */
@Data
public class DictDTO implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 字典名称
     */
    private String name;

    /**
     * 字典编码
     */
    private String code;

    /**
     * 备注
     */
    private String remark;

    /**
     * 版本号
     */
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}