package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 系统菜单表
 */
@Data
public class MenuDTO implements Serializable {

    /**
     * ID
     */
    private String id;

    /**
     * 父级ID
     */
    private String pid;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 菜单介绍
     */
    private String info;

    /**
     * 菜单指向地址
     */
    private String url;

    /**
     * 是否显示
     */
    private Boolean isShow;

    /**
     * 是否外部链接
     */
    private Boolean isBlank;

    /**
     * 菜单类别，1菜单和2按钮，如果存在子菜单则自动变为目录。
     */
    private Integer type;

    /**
     * 菜单级别,最多支持4级菜单也就是说，这个值不能大于4
     */
    private Integer menuLevel;

    /**
     * 权限标识
     */
    private String permission;

    /**
     * 排序
     */
    private Integer sort;


    /**
     * 版本号
     */
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}