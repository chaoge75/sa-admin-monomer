package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统附件表
 */
@Data
public class AttachmentDTO implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 文件名
     */
    private String filename;

    /**
     * 原始文件名
     */
    private String originalFilename;

    /**
     * 文件后缀名
     */
    private String fileType;

    /**
     * 文件大小
     */
    private String fileSize;

    /**
     * 绝对路径
     */
    private String absolutePath;

    /**
     * 网络路径
     */
    private String webPath;

    /**
     * 关联的表名
     */
    private String tableName;
    /**
     * 关联表的字段名
     */
    private String tableField;
    /**
     * 关联表的主键
     */
    private String tablePk;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    @Serial
    private static final long serialVersionUID = 1L;
}