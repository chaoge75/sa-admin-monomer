package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
    * 系统部门表
    */
@Data
public class DeptDTO implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 父级Id
     */
    private String pid;

    /**
     * 名称
     */
    private String name;

    /**
     * 管理人
     */
    private String manage;

    /**
     * 管理人联系电话
     */
    private String managePhone;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态，true启用；false禁用
     */
    private Boolean status;


    /**
     * 版本号
     */
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}