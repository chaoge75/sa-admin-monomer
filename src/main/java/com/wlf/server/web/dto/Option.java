package com.wlf.server.web.dto;

import lombok.Data;

@Data
public class Option {
    // 选项名称
    private String label;
    // 选项值
    private Object value;
    // 是否禁用
    private Boolean disabled;
}
