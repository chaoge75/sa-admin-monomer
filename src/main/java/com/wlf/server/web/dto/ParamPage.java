package com.wlf.server.web.dto;

import lombok.Data;

/**
 * 接收分页的参数
 */
@Data
public class ParamPage {
    //第几页
    private Long current;

    // 一页几条
    private Long size;
}
