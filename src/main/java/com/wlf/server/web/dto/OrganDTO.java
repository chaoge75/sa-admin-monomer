package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
    * 系统机构表
    */
@Data
public class OrganDTO implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 父级机构Id
     */
    private String pid;

    /**
     * 机构名称
     */
    private String name;

    /**
     * 机构类型
     */
    private String type;

    /**
     * 机构类型名称
     */
    private String typeName;

    /**
     * 机构管理人
     */
    private String manage;

    /**
     * 机构管理人联系电话
     */
    private String managePhone;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态，true启用；false禁用
     */
    private Boolean status;

    /**
     * 机构所属区域Id
     */
    private String regionId;
    /**
     * 所属区域信息
     */
    private RegionDTO regionDTO;

    /**
     * 是否查看子集
     *
     */
    private Boolean showChildren;

    /**
     * 版本号
     */
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}