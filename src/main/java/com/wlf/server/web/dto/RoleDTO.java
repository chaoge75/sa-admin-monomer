package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
    * 系统角色表
    */
@Data
public class RoleDTO implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 父ID（暂时不用）
     */
    private String pid;

    /**
     * 角色名
     */
    private String name;

    /**
     * 角色编码
     */
    private String code;

    /**
     * 描述
     */
    private String info;

    /**
     * 状态：true启用；false禁用
     */
    private Boolean status;

    /**
     * 版本号
     */
    private Integer version;


    @Serial
    private static final long serialVersionUID = 1L;
}