package com.wlf.server.web.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
    * 字典数据项
    */
@Data
public class DictInfoDTO implements Serializable {
    /**
     * 注解
     */
    private String id;

    /**
     * 字典编码
     */
    private String  dictCode;
    /**
     * 选项的值
     */
    private String value;

    /**
     * 选项的标签
     */
    private String label;

    /**
     * 是否禁用
     */
    private Boolean disabled;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 版本号
     */
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}