package com.wlf.server.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
    * 系统用户表
    */
@Data
public class UserDTO implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 用户名
     */
    private String name;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 登录账号
     */
    private String loginNo;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 密码加密用的随机字段
     */
    @JsonIgnore
    private String salt;

    /**
     * 登录密码
     */
    @JsonIgnore
    private String password;

    /**
     * 性别，0男，1女，2未知
     */
    private Integer sex;

    /**
     * 用户类别，0系统用户，1普通用户
     */
    private String userType;
    /**
     * typeName
     */
    private String typeName;

    /**
     * 用户状态，true正常，false锁定
     */
    private Boolean status;

    /**
     * 备注
     */
    private String remark;


    /**
     * 最后登陆时间
     */
    private LocalDateTime loginEndTime;

    /**
     * 角色IDs
     */
    private List<String> roleIds;

    /**
     * 拥有的的角色信息
     */
    private List<RoleDTO> roleList;

    /**
     * 角色IDs
     */
    private List<String> menuIds;

    /**
     * 拥有的的角色信息
     */
    private List<MenuDTO> menuList;

    /**
     *  权限码
     */
    private List<String> permissionList;

    /**
     * 用户选择机构ID集合(接收用)
     */
    private List<String> organIds;

    /**
     * 用户所属机构
     */
    private List<OrganDTO> organList;

    /**
     * 用户部门Id合集
     */
    private List<String> deptIds;

    /**
     * 用户拥有部门信息
     */
    private List<DeptDTO> deptList;

    /**
     * 用户部门Id合集
     */
    private List<String> postIds;

    /**
     * 用户拥有部门信息
     */
    private List<PostDTO> postList;



    /**
     * 是否查看子集
     *
     */
    private Boolean showChildren;

    /**
     * 乐观锁版本号
     */
    private Integer version;


    @Serial
    private static final long serialVersionUID = 1L;
}