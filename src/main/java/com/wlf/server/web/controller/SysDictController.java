package com.wlf.server.web.controller;

import com.wlf.server.common.service.SysDictService;
import com.wlf.server.web.dto.DictDTO;
import com.wlf.server.web.dto.DictInfoDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/dict")
public class SysDictController {
    @Resource
    private SysDictService dictService;
    /**
     * 主列表数据
     *
     * @param dto       查询参数
     * @param paramPage 分页
     * @return 数据
     */
    @GetMapping("/list")
    AjaxBean list(DictDTO dto, ParamPage paramPage) {
        return dictService.list(dto, paramPage);
    }

    /**
     * 更新或修改字典数据
     *
     * @param dict 数据实体字符串
     * @return 结果
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(DictDTO dict) {
        return dictService.saveOrUpdate(dict);
    }

    @GetMapping("/del")
    AjaxBean del(String id, String ids) {
        return dictService.del(id, ids);
    }

    @GetMapping("/get")
    AjaxBean get(String id) {
        return dictService.get(id);
    }

    final String PerInfo = "/info";

    /**
     * 主列表数据
     *
     * @param dto       查询参数
     * @param paramPage 分页
     * @return 数据
     */
    @GetMapping(PerInfo + "/list")
    AjaxBean infoList(DictInfoDTO dto, ParamPage paramPage) {
        return dictService.infoList(dto, paramPage);
    }

    /**
     * 更新或修改字典数据
     *
     * @param dict 数据实体字符串
     * @return 结果
     */
    @PostMapping(PerInfo + "/save-or-update")
    AjaxBean infoSaveOrUpdate(DictInfoDTO dict) {
        return dictService.infoSaveOrUpdate(dict);
    }

    @GetMapping(PerInfo + "/del")
    AjaxBean infoDel(String id, String ids) {
        return dictService.infoDel(id, ids);
    }

    @GetMapping(PerInfo + "/get")
    AjaxBean infoGet(String id) {
        return dictService.infoGet(id);
    }

}
