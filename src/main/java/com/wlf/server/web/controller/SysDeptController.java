package com.wlf.server.web.controller;

import com.wlf.server.common.service.SysDeptService;
import com.wlf.server.common.service.SysUserService;
import com.wlf.server.web.dto.DeptDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.dto.UserDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (sys_dept)表控制层
 *
 * @author xxxxx
 */
@RestController
@RequestMapping("/dept")
public class SysDeptController {
    /**
     * 服务对象
     */
    @Resource
    private SysDeptService deptService;
    @Resource
    SysUserService userService;


    /**
     * 主数据
     */
    @GetMapping("/list")
    AjaxBean list(DeptDTO deptDTO) {
        return deptService.list(deptDTO);
    }

    /**
     * 更新或者新增
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(DeptDTO deptDTO) {
        return deptService.saveOrUpdate(deptDTO);
    }

    @GetMapping("/get")
    AjaxBean get(String id){
        return deptService.get(id);
    }
    @GetMapping("/del")
    AjaxBean del(String id){
        return deptService.del(id);
    }
    /**
     * 获取上级列表
     * 上级机构
     * 上级区域
     */
    @GetMapping("/parent-list")
    AjaxBean parentList() {
        return deptService.parentList();
    }

    @GetMapping("/get-user-by-dept-id")
    AjaxBean getUserByOrganId(String id, UserDTO userDTO, ParamPage paramPage){
        return userService.getUserByDeptId(id,userDTO,paramPage);
    }

    /**
     * 获取用户
     */
    @GetMapping("/get-user")
    AjaxBean getUser(String id){
        return userService.get(id);
    }

}
