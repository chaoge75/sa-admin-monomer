package com.wlf.server.web.controller;

import com.wlf.server.common.service.SysOrganService;
import com.wlf.server.common.service.SysRegionService;
import com.wlf.server.web.dto.OrganDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.dto.RegionDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (sys_region)表控制层
 *
 * @author xxxxx
 */
@RestController
@RequestMapping("/region")
public class SysRegionController {
    /**
     * 服务对象
     */
    @Resource
    private SysRegionService regionService;
    @Resource
    private SysOrganService organService;


    @GetMapping("/del")
    AjaxBean del(String id) {
        return regionService.del(id);
    }

    @GetMapping("/get")
    AjaxBean get(String id) {
        return regionService.get(id);
    }

    /**
     * 更新或者新增
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(RegionDTO regionDTO) {
        return regionService.saveOrUpdate(regionDTO);
    }

    /**
     * 页面右侧机构数据，根据区域Id查的，分为两种情况，查看子集，和不查看子集
     */
    @GetMapping("/get-organ-by-region-id")
    AjaxBean getOrganByRegion(String id, OrganDTO organDTO, ParamPage paramPage) {
        return organService.getOrganByRegionId(id, organDTO, paramPage);
    }

    @GetMapping("/get-organ")
    AjaxBean getOrgan(String id){
        return organService.get(id);
    }

    /**
     * 获取上级列表
     */
    @GetMapping("/parent-list")
    AjaxBean parentList() {
        return regionService.parentList();
    }

    /**
     * 主数据
     */
    @GetMapping("/list")
    AjaxBean list(RegionDTO regionDTO) {
        return regionService.list(regionDTO);
    }
}

