package com.wlf.server.web.controller;

import com.wlf.server.common.service.SysPostService;
import com.wlf.server.common.service.SysUserService;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.dto.PostDTO;
import com.wlf.server.web.dto.UserDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (sys_post)表控制层
 *
 * @author xxxxx
 */
@RestController
@RequestMapping("/post")
public class SysPostController {
    /**
     * 服务对象
     */
    @Resource
    private SysPostService postService;
    @Resource
    private SysUserService userService;


    /**
     * 主数据
     */
    @GetMapping("/list")
    AjaxBean list(PostDTO postDTO) {
        return postService.list(postDTO);
    }

    /**
     * 更新或者新增
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(PostDTO postDTO) {
        return postService.saveOrUpdate(postDTO);
    }

    @GetMapping("/get")
    AjaxBean get(String id){
        return postService.get(id);
    }
    @GetMapping("/del")
    AjaxBean del(String id){
        return postService.del(id);
    }
    /**
     * 获取上级列表
     * 上级机构
     * 上级区域
     */
    @GetMapping("/parent-list")
    AjaxBean parentList() {
        return postService.parentList();
    }

    @GetMapping("/get-user-by-post-id")
    AjaxBean getUserByOrganId(String id, UserDTO userDTO, ParamPage paramPage){
        return userService.getUserByPostId(id,userDTO,paramPage);
    }

    /**
     * 获取用户
     */
    @GetMapping("/get-user")
    AjaxBean getUser(String id){
        return userService.get(id);
    }


}
