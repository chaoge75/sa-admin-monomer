package com.wlf.server.web.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wlf.server.common.service.SysAttachmentService;
import com.wlf.server.web.dto.AttachmentDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 *
 */
@RestController
@RequestMapping("/attachment")
public class SysAttachmentController {
    /**
     * 服务对象
     */
    @Resource
    private SysAttachmentService attachmentService;


    /**
     * 主列表
     */
    @GetMapping("/list")
    AjaxBean list(AttachmentDTO attachmentDTO, ParamPage paramPage){

        return AjaxBean.getOkPage(attachmentService.page(attachmentDTO,Page.of(paramPage.getCurrent(), paramPage.getSize())));
    }

    /**
     * 上传文件
     */
    @PostMapping("/plus")
    AjaxBean plus(MultipartFile file){
        return AjaxBean.getOkMsg("上传成功",attachmentService.plus(file));
    }

    /**
     * 读取文件
     */
    @GetMapping("/read")
    void read(String id){
        attachmentService.read(id);
    }

    /**
     * 删除文件
     */
    @PostMapping("del")
    AjaxBean del(String id,String ids){
        attachmentService.del(id,ids);
        return AjaxBean.getOkMsg("删除成功");
    }
}
