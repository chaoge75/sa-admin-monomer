package com.wlf.server.web.controller;

import com.wlf.server.common.service.SysApiLogService;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * API请求日志 前端控制器
 * </p>
 *
 * @author 清欢
 * @since 2022-09-06
 */
@RestController
@RequestMapping("/api-log")
public class SysApiLogController {

    @Resource
    private SysApiLogService apiLogService;

    /**
     * 列表分页数据
     * @param name 模糊条件
     * @param paramPage 分页
     * @return 数据
     */
    @GetMapping("/list")
    AjaxBean list(String name, ParamPage paramPage) {
        return apiLogService.list(name,paramPage);
    }

    /**
     * 获取单条记录详情
     * @param id 主键
     * @return 记录
     */
    @GetMapping("/get")
    AjaxBean get(String id){
        return apiLogService.get(id);
    }

    /**
     * 删除
     *
     * @param id  主键
     * @param ids 主键集合字符串
     * @return 记录
     */
    @PostMapping("del")
    AjaxBean del(String id, String ids) {
        return apiLogService.del(id,ids);
    }

}
