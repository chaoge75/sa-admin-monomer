package com.wlf.server.web.controller;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.wlf.server.common.entity.RoleMenu;
import com.wlf.server.common.entity.SysMenu;
import com.wlf.server.common.entity.SysRole;
import com.wlf.server.common.entity.SysUser;
import com.wlf.server.common.service.SysMenuService;
import com.wlf.server.common.service.SysRoleService;
import com.wlf.server.common.service.SysUserService;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.dto.RoleDTO;
import com.wlf.server.web.util.AjaxBean;
import com.wlf.server.web.util.JSON;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色
 */

@RestController
@RequestMapping("/role")
public class SysRoleController {
    @Resource
    SysRoleService roleService;


    @Resource
    SysMenuService menuService;
    @Resource
    SysUserService userService;

    @GetMapping("/list")
    AjaxBean list(RoleDTO roleDTO,  ParamPage paramPage) {
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(roleDTO.getName())) {
            wrapper.like(SysRole::getName, roleDTO.getName());
            wrapper.or().like(SysRole::getInfo, roleDTO.getName());
        }

        List<SysMenu> list = menuService.list();
        TreeNodeConfig tConfig = new TreeNodeConfig();
        // 自定义属性名 都要默认值的
        tConfig.setIdKey("id");
        tConfig.setChildrenKey("children");
        tConfig.setNameKey("label");

        Dict dict = Dict.create();
        dict.set("menuData", menuService.getTree(list, tConfig));
        dict.set("roleData", AjaxBean.getOkPage(roleService.page(Page.of(paramPage.getCurrent(), paramPage.getSize()), wrapper)));

        return AjaxBean.getOkData(dict);
    }

    @GetMapping("/get")
    AjaxBean get(String id) {
        return roleService.get(id);
    }

    @GetMapping("/get-menu")
    AjaxBean getMenu(String id) {
        return menuService.get(id);
    }

    @GetMapping("/get-user")
    AjaxBean getUser(String id){
        return userService.get(id);
    }

    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(RoleDTO roleDTO) {
        return roleService.saveOrUpdate(roleDTO);
    }

    @GetMapping("/del")
    AjaxBean del(String id, String ids) {
        if (StrUtil.isBlank(id)) {
            return AjaxBean.getByBool(roleService.removeBatchByIds(JSON.parseArray(ids, String.class)));
        }
        return AjaxBean.getByBool(roleService.removeById(id));
    }

    /**
     * 根据角色ID加载对应的信息
     */
    @GetMapping("/init-role-info")
    AjaxBean initRoleInfo(String id) {
        List<RoleMenu> list = Db.lambdaQuery(RoleMenu.class).eq(RoleMenu::getRoleId, id).list();
        List<String> menuIds = list.stream().map(RoleMenu::getMenuId).collect(Collectors.toList());
        Dict dict = Dict.create();
        dict.set("menuIds", menuIds);
        return AjaxBean.getOkData(dict);
    }

    @PostMapping("/sava-role-menu")
    AjaxBean savaRoleMenu(String id, String menuIds) {
        return roleService.savaRoleMenu(id,menuIds);
    }


    @GetMapping("/user-list")
    AjaxBean userList() {
        List<SysUser> list = userService.list();
        List<Dict> data = list.stream().map(i ->
                Dict.create().set("key", i.getId()).set("label", i.getName())).collect(Collectors.toList());
        return AjaxBean.getOkData(data);
    }

    /**
     * 根据角色ID查找拥有此角色的用户
     */
    @GetMapping("/user-list-by-role-id")
    AjaxBean userListByRoleId(String id, Page<SysUser> page) {
        return userService.userList(id, page);
    }

    @PostMapping("/change-role-user")
    AjaxBean changeRoleUser(String id, String userIds) {
        return roleService.changeRoleUser(id, userIds);
    }
}
