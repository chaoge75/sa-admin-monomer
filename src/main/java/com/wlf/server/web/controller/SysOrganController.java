package com.wlf.server.web.controller;

import com.wlf.server.common.service.SysOrganService;
import com.wlf.server.common.service.SysUserService;
import com.wlf.server.web.dto.OrganDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.dto.UserDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (sys_organ)表控制层
 *
 * @author xxxxx
 */
@RestController
@RequestMapping("/organ")
public class SysOrganController {
    /**
     * 服务对象
     */
    @Resource
    private SysOrganService organService;
    @Resource
    private SysUserService userService;

    /**
     * 主数据
     */
    @GetMapping("/list")
    AjaxBean list(OrganDTO organDTO) {
        return organService.list(organDTO);
    }

    /**
     * 更新或者新增
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(OrganDTO organDTO) {
        return organService.saveOrUpdate(organDTO);
    }

    @GetMapping("/get")
    AjaxBean get(String id){
        return organService.get(id);
    }
    @GetMapping("/del")
    AjaxBean del(String id){
        return organService.del(id);
    }
    /**
     * 获取上级列表
     * 上级机构
     * 上级区域
     */
    @GetMapping("/parent-list")
    AjaxBean parentList() {
        return organService.parentList();
    }

    @GetMapping("/get-user-by-organ-id")
    AjaxBean getUserByOrganId(String id, UserDTO userDTO, ParamPage paramPage){
        return userService.getUserByOrganId(id,userDTO,paramPage);
    }

    /**
     * 获取用户
     */
    @GetMapping("/get-user")
    AjaxBean getUser(String id){
        return userService.get(id);
    }
}
