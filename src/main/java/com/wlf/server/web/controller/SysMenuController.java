package com.wlf.server.web.controller;

import cn.hutool.core.lang.tree.TreeNodeConfig;
import com.wlf.server.common.entity.SysMenu;
import com.wlf.server.common.service.SysMenuService;
import com.wlf.server.web.dto.MenuDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 提供后台的请求接口
 */
@RestController
@RequestMapping("/menu")
public class SysMenuController {
    @Resource
    SysMenuService menuService;

    /**
     * 全部菜单
     */

    @GetMapping("/list")
    AjaxBean list(){
        List<SysMenu> list = menuService.list();
        TreeNodeConfig tConfig = new TreeNodeConfig();
        // 自定义属性名 都要默认值的
        tConfig.setIdKey("id");
        tConfig.setChildrenKey("children");
        tConfig.setNameKey("name");
        tConfig.setParentIdKey("pid");
        // 最大递归深度
        tConfig.setDeep(5);
        return AjaxBean.getOkData(menuService.getTree(list,tConfig));
    }

    /**
     * 修改或更新菜单
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(MenuDTO menuDTO){
        return menuService.saveOrUpdate(menuDTO);
    }

    /**
     * 获取上级菜单
     */
    @GetMapping("/parentList")
    AjaxBean parentList(){
        return menuService.parentList();
    }

    /**
     * 根据ID获取
     */
    @GetMapping("/get")
    AjaxBean byId(String id){
        return menuService.get(id);
    }

    /**
     * 删除
     */
    @GetMapping("/del")
    AjaxBean del(String id){
        // 删除父级时会连代删除子级
        return menuService.remove(id);
    }

    /**
     * 导入菜单文件
     */
    @GetMapping("/export")
    void export(){
       menuService.exportMenu();
    }

    /**
     * 导入菜单数据
     */
    @PostMapping("/import-menu")
    AjaxBean importMenu(MultipartFile file){
        return menuService.importMenu(file);
    }
}
