package com.wlf.server.web.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.StrUtil;
import com.wlf.server.common.em.Const;
import com.wlf.server.common.entity.SysUser;
import com.wlf.server.common.service.SysUserService;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.dto.UserDTO;
import com.wlf.server.web.util.AjaxBean;
import com.wlf.server.web.util.JSON;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Arrays;

@RestController
@RequestMapping("/user")
public class SysUserController {
    @Resource
    SysUserService userService;

    /**
     * 主列表数据
     */
    @GetMapping("/list")
    AjaxBean list(UserDTO userDTO, ParamPage paramPage) {
        return userService.list(userDTO, paramPage);

    }

    /**
     * 更新或新增用户
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(String userDTO) {
        return userService.saveOrUpdate(JSON.parseObject(userDTO, UserDTO.class));
    }

    /**
     * 更新状态
     */
    @PostMapping("/update-status")
    AjaxBean updateStatus(UserDTO userDTO) {
        return userService.updateStatus(userDTO);
    }

    /**
     * 根据ID获取用户
     */
    @GetMapping("/get")
    AjaxBean get(String id) {
        return userService.get(id);
    }

    /**
     * 删除用户
     */
    @PostMapping("/del")
    AjaxBean del(String id, String ids) {
        return StrUtil.isBlank(id) ?
                userService.delAll(ids) :
                userService.del(id);
    }

    /**
     * 重置密码
     */
    @GetMapping("/reset")
    AjaxBean reset(String id) {
        SysUser user = new SysUser();
        user.setId(id);
        user.setPassword(SysUserService.passRes);
        return AjaxBean.getByBool(userService.updateById(user));
    }


    /**
     * 修改密码
     */
    @GetMapping("/change-pas")
    AjaxBean changePas(String oldPas, String newPas) {
        return userService.changePas(oldPas, newPas);
    }

    /**
     * 初始化表单中的选项信息，如角色列表，机构列表等
     */
    @GetMapping("/init-form")
    AjaxBean initForm() {
        Dict data = Dict.create();
        data.set("roleList", userService.getRoleList());
        data.set("organList", userService.getOrganList());
        data.set("postList", userService.getPostList());
        data.set("deptList", userService.getDeptList());
        data.set("typeOptions", Arrays.stream(Const.UserType.values()).map(
                i -> Dict.create().set("label", i.getDesc()).set("value", i.name())
        ).toList());
        return AjaxBean.getOkData(data);
    }


    /**
     * 获取当前登录用户
     */
    @GetMapping("/get-login")
    AjaxBean getLogin() {
        return AjaxBean.getOkData(userService.getUserByLogin());
    }

    /**
     * 上传头像
     */
    @PostMapping("/plus-avatar")
    AjaxBean plusAvatar(MultipartFile file) {
        return AjaxBean.getOkData(userService.plusAvatar(file,
                StpUtil.getLoginIdAsString()));
    }
}
