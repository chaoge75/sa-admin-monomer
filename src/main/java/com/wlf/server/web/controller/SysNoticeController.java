package com.wlf.server.web.controller;

import cn.hutool.core.lang.Dict;
import com.wlf.server.common.entity.SysAttachment;
import com.wlf.server.common.service.SysAttachmentService;
import com.wlf.server.common.service.SysNoticeService;
import com.wlf.server.web.dto.NoticeDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 系统公告控制器
 */
@RestController
@RequestMapping("/notice")
public class SysNoticeController {
    @Resource
    private SysNoticeService noticeService;
    @Resource
    private SysAttachmentService attachmentService;

    /**
     * 图片上传
     *
     * @param file 文件
     * @return 返回
     */
    @PostMapping("/upload-image")
    AjaxBean imageUpload(MultipartFile file) {
        SysAttachment attachment = attachmentService.plus(file, "通知公告图片");
        AjaxBean bean = AjaxBean.getOkMsg();
        bean.set("errno", 0);
        return bean.setData(Dict.create()
                .set("url", attachment.getWebPath())
                .set("alt", attachment.getOriginalFilename())
        );
    }

    /**
     * 更新或者新增公告
     *
     * @param dto 公告实体
     * @return 操作结果
     */
    @PostMapping("/save-or-update")
    AjaxBean saveOrUpdate(NoticeDTO dto) {
        return noticeService.saveOrUpdate(dto);
    }

    /**
     * 公告管理主界面
     *
     * @param dto       查询参数
     * @param paramPage 分页
     * @return 数据
     */
    @GetMapping("/list")
    AjaxBean list(NoticeDTO dto, ParamPage paramPage) {
        return noticeService.list(dto, paramPage);
    }

    /**
     * 获取单个通知公告
     * @param id 主键
     * @return 主体
     */
    @GetMapping("/get")
    AjaxBean get(String id){
        return noticeService.get(id);
    }
    /**
     * 删除公告
     *
     * @param id 公告主键
     * @return 操作结果
     */
    @PostMapping("/del")
    AjaxBean del(String id) {
        return noticeService.del(id);
    }

    /**
     * 加载工具栏中的通知列表
     * @return 通知列表
     */
    @GetMapping("/get-tool-list")
    AjaxBean getToolList(){
        return noticeService.getToolList();
    }

    /**
     * 已读公告
     * @param id 通知ID
     * @return 操作结果
     */
    @GetMapping("/reading")
    AjaxBean reading(String id){
        return noticeService.reading(id);
    }

}
