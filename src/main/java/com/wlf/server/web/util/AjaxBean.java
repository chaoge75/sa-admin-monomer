package com.wlf.server.web.util;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serial;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;



/**
 * ajax请求返回Json格式数据的封装 <br>
 * 所有预留字段：<br>
 * code=状态码 <br>
 * msg=描述信息 <br>
 * data=携带对象 <br>
 * pageNo=当前页 <br>
 * pageSize=页大小 <br>
 * startIndex=起始索引 <br>
 * dataCount=数据总数 <br>
 * pageCount=分页总数 <br>
 * <p> 返回范例：</p>
 *  <pre>
 {
 "code": 200,    // 成功时=200, 失败时=500  msg=失败原因
 "msg": "ok",
 "data": {}
 }
 </pre>
 */
public class AjaxBean extends LinkedHashMap<String, Object> implements Serializable{

    @Serial
    private static final long serialVersionUID = 1L;	// 序列化版本号

    public static final int CODE_SUCCESS = 200;			// 成功状态码
    public static final int CODE_ERROR = 500;			// 错误状态码
    public static final int CODE_WARNING = 501;			// 警告状态码
    public static final int CODE_NOT_JUR = 403;			// 无权限状态码
    public static final int CODE_NOT_LOGIN = 401;		// 未登录状态码

    public static final int CODE_INVALID_REQUEST = 400;	// 无效请求状态码



    // ============================  写值取值  ==================================

    /** 给code赋值，连缀风格 */
    public AjaxBean setCode(int code) {
        this.put("code", code);
        return this;
    }
    /** 返回code */
    public Integer getCode() {
        return (Integer)this.get("code");
    }

    /** 给msg赋值，连缀风格 */
    public AjaxBean setMsg(String msg) {
        this.put("msg", msg);
        return this;
    }
    /** 获取msg */
    public String getMsg() {
        return (String)this.get("msg");
    }

    /** 给data赋值，连缀风格 */
    public AjaxBean setData(Object data) {
        this.put("data", data);
        return this;
    }
    /** 获取data */
    public Object getData() {
        return this.get("data");
    }


    /** 给dataCount(数据总数)赋值，连缀风格 */
    public AjaxBean setDataCount(Long dataCount) {
        this.put("dataCount", dataCount);
        return this;
    }
    /** 获取dataCount(数据总数) */
    public Long getDataCount() {
        return (long)this.get("dataCount");
    }

    /** 设置pageNo 和 pageSize，并计算出startIndex于pageCount */
    public AjaxBean setPageNoAndSize(long pageNo, long pageSize) {
        this.put("pageNo", pageNo);
        this.put("pageSize", pageSize);
        return this;
    }

    /** 根据 pageSize dataCount，计算startIndex 与 pageCount */
    public void initPageInfo() {
        long pageNo = (long)this.get("pageNo");
        long pageSize = (long)this.get("pageSize");
        long dataCount = (long)this.get("dataCount");
        long pc = dataCount / pageSize;
        this.set("startIndex", (pageNo - 1) * pageSize).set("pageCount", (dataCount % pageSize == 0 ?  pc : pc + 1));
    }


    /** 写入一个值 自定义key, 连缀风格 */
    public AjaxBean set(String key, Object data) {
        this.put(key, data);
        return this;
    }

    /** 写入一个Map, 连缀风格 */
    public AjaxBean setMap(Map<String, ?> map) {
        for (String key : map.keySet()) {
            this.put(key, map.get(key));
        }
        return this;
    }


    // ============================  构建  ==================================

    public AjaxBean(int code, String msg, Object data, Long dataCount) {
        this.setCode(code);
        this.setMsg(msg);
        this.setData(data);
        this.setDataCount(dataCount == null ? -1 : dataCount);
    }

    /** 返回成功 */
    public static AjaxBean getOkMsg() {
        return new AjaxBean(CODE_SUCCESS, "ok", null, null);
    }
    public static AjaxBean getOkMsg(String msg) {
        return new AjaxBean(CODE_SUCCESS, msg, null, null);
    }
    public static AjaxBean getOkMsg(String msg, Object data) {
        return new AjaxBean(CODE_SUCCESS, msg, data, null);
    }
    public static AjaxBean getOkData(Object data) {
        return new AjaxBean(CODE_SUCCESS, "ok", data, null);
    }
    public static AjaxBean getOkPage(Page<?> page) {
        return new AjaxBean(CODE_SUCCESS, "ok", page.getRecords(), page.getTotal());
    }


    /** 返回失败 */
    public static AjaxBean getError() {
        return new AjaxBean(CODE_ERROR, "error", null, null);
    }
    public static AjaxBean getError(String msg) {
        return new AjaxBean(CODE_ERROR, msg, null, null);
    }

    /** 返回警告  */
    public static AjaxBean getWarning() {
        return new AjaxBean(CODE_ERROR, "warning", null, null);
    }
    public static AjaxBean getWarning(String msg) {
        return new AjaxBean(CODE_WARNING, msg, null, null);
    }

    /** 返回未登录  */
    public static AjaxBean getNotLogin() {
        return new AjaxBean(CODE_NOT_LOGIN, "未登录，请登录后再次访问", null, null);
    }

    /** 返回没有权限的  */
    public static AjaxBean getNotJur(String msg) {
        return new AjaxBean(CODE_NOT_JUR, msg, null, null);
    }

    /** 返回一个自定义状态码的  */
    public static AjaxBean get(int code, String msg){
        return new AjaxBean(code, msg, null, null);
    }

    /** 返回分页和数据的  */
    public static AjaxBean getPageData(Long dataCount, Object data){
        return new AjaxBean(CODE_SUCCESS, "ok", data, dataCount);
    }

    /** 返回, 根据受影响行数的(大于0=ok，小于0=error)  */
    public static AjaxBean getByLine(int line){
        if(line > 0){
            return getOkMsg("ok", line);
        }
        return getError("error").setData(line);
    }

    /** 返回，根据布尔值来确定最终结果的  (true=ok，false=error)  */
    public static AjaxBean getByBool(boolean b){
        return b ? getOkMsg("ok") : getError("error");
    }

    /**
     *
     * @param b 是否成功的布尔值
     * @param sucMsg 成功返回的信息
     * @param failMsg 失败返回的信息
     */
    public static AjaxBean getByBool(boolean b,String sucMsg,String failMsg){
        if (b){
            return getOkMsg(StrUtil.isBlank(sucMsg)?"ok":sucMsg);
        } else {
            return getError(StrUtil.isBlank(failMsg)?"error":failMsg);
        }
    }


}

