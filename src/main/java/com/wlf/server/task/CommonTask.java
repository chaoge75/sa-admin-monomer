package com.wlf.server.task;


import com.wlf.server.common.em.Const;
import com.wlf.server.common.quartz.base.BaseQuartz;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时任务加上这个Const.TaskBeanKey可以在前台中获得输入建议
 */
@Slf4j
@Component(Const.TaskBeanKey+"commonTask")
public class CommonTask implements BaseQuartz {

    private final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * 任务实现
     */
    @Override
    public void run(String params) {
        log.info("Params === >> " + params);
        log.info("当前时间::::" + FORMAT.format(new Date()));
        System.out.println("执行成功");
    }
}
