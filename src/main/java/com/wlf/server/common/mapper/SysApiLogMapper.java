package com.wlf.server.common.mapper;

import com.wlf.server.common.entity.SysApiLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * API请求日志 Mapper 接口
 * </p>
 *
 * @author 清欢
 * @since 2022-09-06
 */
@Mapper
public interface SysApiLogMapper extends BaseMapper<SysApiLog> {

}
