package com.wlf.server.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlf.server.common.entity.SysDictInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDictInfoMapper extends BaseMapper<SysDictInfo> {
}