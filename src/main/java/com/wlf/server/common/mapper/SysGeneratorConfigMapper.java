package com.wlf.server.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlf.server.common.entity.SysGeneratorConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 代码生成配置表 Mapper 接口
 * </p>
 *
 * @author 清欢
 * @since 2022-09-07
 */
@Mapper
public interface SysGeneratorConfigMapper extends BaseMapper<SysGeneratorConfig> {

}
