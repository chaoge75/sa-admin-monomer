package com.wlf.server.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlf.server.common.entity.SysSerial;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysSerialMapper extends BaseMapper<SysSerial> {
}