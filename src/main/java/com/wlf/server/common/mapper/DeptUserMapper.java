package com.wlf.server.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlf.server.common.entity.DeptUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DeptUserMapper extends BaseMapper<DeptUser> {
}