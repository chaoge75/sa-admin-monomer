package com.wlf.server.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlf.server.common.entity.SysRegion;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRegionMapper extends BaseMapper<SysRegion> {
}