package com.wlf.server.common.context;

import org.springframework.stereotype.Component;

/**
 * 用户上下文，这个上下文不用用在WebSocket里面喔
 * <p>
 * @serial 2.0.0
 * @author 就眠儀式
 */
@Component
public class UserContext {
}
