package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统用户表
 */
@Data
@TableName(value = "sys_user")
public class SysUser implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 用户名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 用户头像
     */
    @TableField(value = "avatar")
    private String avatar;

    /**
     * 登录账号
     */
    @TableField(value = "login_no")
    private String loginNo;

    /**
     * 手机号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 密码加密用的随机字段
     */
    @TableField(value = "salt")
    private String salt;

    /**
     * 登录密码
     */
    @TableField(value = "`password`")
    private String password;

    /**
     * 性别，0男，1女，2未知
     */
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 用户类别，SYS系统用户，NORM正常用户
     */
    @TableField(value = "`user_type`")
    private String userType;

    /**
     * 用户状态，0正常，1锁定
     */
    @TableField(value = "`status`")
    private Boolean status;

    /**
     * 备注
     */
    @TableField(value = "`remark`")
    private String remark;


    /**
     * 最后登陆时间
     */
    @TableField(value = "login_end_time")
    private LocalDateTime loginEndTime;

    /**
     * 更新人
     */
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 乐观锁版本号
     */
    @Version
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}