package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import lombok.Data;

@Data
@TableName(value = "dept_user")
public class DeptUser implements Serializable {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @TableField(value = "dept_id")
    private String deptId;

    @TableField(value = "user_id")
    private String userId;

    @Serial
    private static final long serialVersionUID = 1L;
}