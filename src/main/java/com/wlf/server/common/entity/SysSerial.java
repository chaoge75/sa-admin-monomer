package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
    * 自定义序列表
    */
@Data
@TableName(value = "sys_serial")
public class SysSerial implements Serializable {
    @TableId(value = "serial_type", type = IdType.INPUT)
    private String serialType;

    /**
     * 序列号
     */
    @TableField(value = "serial_no")
    private Integer serialNo;

    /**
     * 生成时间
     */
    @TableField(value = "generate_date")
    private String generateDate;

    private static final long serialVersionUID = 1L;
}