package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 系统附件表
 */
@Data
@TableName(value = "sys_attachment")
public class SysAttachment implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 文件名
     */
    @TableField(value = "filename")
    private String filename;

    /**
     * 原始文件名
     */
    @TableField(value = "original_filename")
    private String originalFilename;

    /**
     * 文件后缀名
     */
    @TableField(value = "file_type")
    private String fileType;

    /**
     * 文件大小
     */
    @TableField(value = "file_size")
    private String fileSize;

    /**
     * 绝对路径
     */
    @TableField(value = "absolute_path")
    private String absolutePath;

    /**
     * 网络路径
     */
    @TableField(value = "web_path")
    private String webPath;

    /**
     * 文件存储位置
     */
    @TableField(value = "file_src")
    private String fileSrc;

    /**
     * 关联的表名
     */
    @TableField(value = "table_name")
    private String tableName;
    /**
     * 关联表的字段名
     */
    @TableField(value = "table_field")
    private String tableField;
    /**
     * 关联表的主键
     */
    @TableField(value = "table_pk")
    private String tablePk;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 创建人
     */
    @TableField(value = "create_by")
    private String createBy;

    @Serial
    private static final long serialVersionUID = 1L;
}