package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
    * 岗位用户关联表
    */
@Data
@TableName(value = "post_user")
public class PostUser implements Serializable {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @TableField(value = "post_id")
    private String postId;

    @TableField(value = "user_id")
    private String userId;

    private static final long serialVersionUID = 1L;
}