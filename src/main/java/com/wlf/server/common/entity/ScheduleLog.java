package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

/**
    * 定时任务日志
    */
@Data
@TableName(value = "schedule_log")
public class ScheduleLog implements Serializable {
    /**
     * 任务日志id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 任务id
     */
    @TableField(value = "job_id")
    private String jobId;

    /**
     * 任务名称
     */
    @TableField(exist = false)
    private String jobName;

    /**
     * spring bean名称
     */
    @TableField(value = "bean_name")
    private String beanName;

    /**
     * 参数
     */
    @TableField(value = "params")
    private String params;

    /**
     * 任务状态    0：成功    1：失败
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 失败信息
     */
    @TableField(value = "error")
    private String error;

    /**
     * 耗时(单位：毫秒)
     */
    @TableField(value = "times")
    private Integer times;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 用来接收日期区间参数
     */
    @TableField(exist = false)
    private List<String> createTimeList;

    @Serial
    private static final long serialVersionUID = 1L;
}