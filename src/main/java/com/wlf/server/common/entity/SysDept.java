package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
    * 系统部门表
    */
@Data
@TableName(value = "sys_dept")
public class SysDept implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 父级Id
     */
    @TableField(value = "pid")
    private String pid;

    /**
     * 名称
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 管理人
     */
    @TableField(value = "manage")
    private String manage;

    /**
     * 管理人联系电话
     */
    @TableField(value = "manage_phone")
    private String managePhone;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 排序
     */
    @TableField(value = "sort")
    private Integer sort;

    /**
     * 状态，true启用；false禁用
     */
    @TableField(value = "`status`")
    private Boolean status;


    /**
     * 更新人
     */
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 乐观锁版本号
     */
    @Version
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}