package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import lombok.Data;

/**
    * 机构用户关联表
    */
@Data
@TableName(value = "organ_user")
public class OrganUser implements Serializable {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @TableField(value = "organ_id")
    private String organId;

    @TableField(value = "user_id")
    private String userId;

    @Serial
    private static final long serialVersionUID = 1L;
}