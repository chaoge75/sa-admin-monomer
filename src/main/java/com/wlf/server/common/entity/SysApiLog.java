package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * API请求日志
 * </p>
 *
 * @author 清欢
 * @since 2022-09-06
 */
@Getter
@Setter
@TableName("sys_api_log")
public class SysApiLog implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 请求用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 请求地址
     */
    @TableField("url")
    private String url;

    /**
     * 请求方法
     */
    @TableField("method")
    private String method;

    /**
     * get请求参数
     */
    @TableField("query_string")
    private String queryString;

    /**
     * 请求表单数据
     */
    @TableField("form_data")
    private String formData;

    /**
     * 请求JSON数据
     */
    @TableField("json_data")
    private String jsonData;

    /**
     * 请求时间
     */
    @TableField("request_time")
    private LocalDateTime requestTime;

    /**
     * 返回code
     */
    @TableField("response_code")
    private String responseCode;
}
