package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 代码生成配置表
 * </p>
 *
 * @author 清欢
 * @since 2022-09-07
 */
@Getter
@Setter
@TableName("sys_generator_config")
public class SysGeneratorConfig implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID，主键
     */
    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    private String userId;

    /**
     * 代码生成配置
     */
    @TableField("generator_config")
    private String generatorConfig;
}
