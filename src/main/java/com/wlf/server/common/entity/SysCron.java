package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serial;
import java.io.Serializable;
import lombok.Data;

/**
    * 常用定时任务表达式
    */
@Data
@TableName(value = "sys_cron")
public class SysCron implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * cron表达式
     */
    @TableField(value = "expression")
    private String expression;

    /**
     * 描述
     */
    @TableField(value = "`desc`")
    private String desc;

    @Serial
    private static final long serialVersionUID = 1L;
}