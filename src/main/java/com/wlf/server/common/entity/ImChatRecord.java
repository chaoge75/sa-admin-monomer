package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
    * 聊天记录表
    */
@Data
@TableName(value = "im_chat_record")
public class ImChatRecord implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 发送消息人ID
     */
    @TableField(value = "send_user_id")
    private String sendUserId;

    /**
     * 接收消息人ID
     */
    @TableField(value = "receive_user_id")
    private String receiveUserId;

    /**
     * 消息主体
     */
    @TableField(value = "msg_body")
    private String msgBody;

    /**
     * 消息发送时间
     */
    @TableField(value = "send_time")
    private LocalDateTime sendTime;


    /**
     * 是否已读
     */
    @TableField(value = "reading")
    private Boolean reading;

    @Serial
    private static final long serialVersionUID = 1L;
}