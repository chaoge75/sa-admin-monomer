package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 字典数据项
 */
@Data
@TableName(value = "sys_dict_info")
public class SysDictInfo implements Serializable {
    /**
     * 注解
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 字典编码
     */
    @TableField("dict_code")
    private String dictCode;
    /**
     * 选项的值
     */
    @TableField(value = "`value`")
    private String value;

    /**
     * 选项的标签
     */
    @TableField(value = "`label`")
    private String label;

    /**
     * 是否禁用
     */
    @TableField(value = "disabled")
    private Boolean disabled;

    /**
     * 排序
     */
    @TableField(value = "sort")
    private Integer sort;

    /**
     * 创建人
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 版本号
     */
    @Version
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}