package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
    * 系统角色表
    */
@Data
@TableName(value = "sys_role")
public class SysRole implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 父ID（暂时不用）
     */
    @TableField(value = "pid")
    private String pid;

    /**
     * 角色名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 角色编码
     */
    @TableField(value = "`code`")
    private String code;

    /**
     * 描述
     */
    @TableField(value = "info")
    private String info;

    /**
     * 状态：true启用；false禁用
     */
    @TableField(value = "`status`")
    private Boolean status;

    /**
     * 创建人
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 版本号
     */
    @Version
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}