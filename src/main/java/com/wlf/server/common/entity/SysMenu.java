package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
    * 系统菜单表
    */
@Data
@TableName(value = "sys_menu")
public class SysMenu implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 父级ID
     */
    @TableField(value = "pid")
    private String pid;

    /**
     * 菜单名称
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 菜单图标
     */
    @TableField(value = "icon")
    private String icon;

    /**
     * 菜单介绍
     */
    @TableField(value = "info")
    private String info;

    /**
     * 菜单指向地址
     */
    @TableField(value = "url")
    private String url;

    /**
     * 是否显示
     */
    @TableField(value = "is_show")
    private Boolean isShow;

    /**
     * 是否外部链接
     */
    @TableField(value = "is_blank")
    private Boolean isBlank;

    /**
     * 菜单类别，1菜单和2按钮，如果存在子菜单则自动变为目录。
     */
    @TableField(value = "`type`")
    private Integer type;

    /**
     * 菜单级别,最多支持4级菜单也就是说，这个值不能大于4
     */
    @TableField(value = "menu_level")
    private Integer menuLevel;

    /**
     * 权限标识
     */
    @TableField(value = "permission")
    private String permission;

    /**
     * 排序
     */
    @TableField(value = "sort")
    private Integer sort;


    /**
     * 更新人
     */
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 乐观锁版本号
     */
    @Version
    private Integer version;

    @Serial
    private static final long serialVersionUID = 1L;
}