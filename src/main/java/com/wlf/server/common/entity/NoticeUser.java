package com.wlf.server.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
    * 通知用户关联表
    */
@Data
@TableName(value = "notice_user")
public class NoticeUser implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 公告ID
     */
    @TableField(value = "notice_id")
    private String noticeId;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private String userId;

    /**
     * 已读时间
     */
    @TableField(value = "read_time")
    private LocalDateTime readTime;

    private static final long serialVersionUID = 1L;
}