package com.wlf.server.common.run;

import com.wlf.server.common.quartz.QuartzStarted;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 项目启动后执行方法
 * --@PostConstruct > InitializingBean > ApplicationRunner > CommandLineRunner
 *
 */

@Component
public class PostRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        // 定时任务初始化
        QuartzStarted.init();
    }
}
