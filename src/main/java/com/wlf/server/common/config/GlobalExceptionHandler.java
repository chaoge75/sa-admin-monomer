package com.wlf.server.common.config;

import cn.dev33.satoken.exception.DisableServiceException;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import cn.hutool.core.util.StrUtil;
import com.wlf.server.web.util.AjaxBean;
import com.wlf.server.web.util.AjaxError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * 全局异常捕获
 * 顺序不可写错，接口越高地拦截，就越排在最后
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {



    /**
     * 自定义异常
     */
    @ExceptionHandler(AjaxError.class)
    AjaxBean handlerAjaxError(AjaxError e){
        e.printStackTrace();
        log.info("自定义异常: {}",e.getMessage());
        return AjaxBean.getError(e.getMessage());
    }

    @ExceptionHandler(NotLoginException.class)
    AjaxBean handlerNotLoginException(NotLoginException e){
        // 如果是未登录异常
        e.printStackTrace();
        log.info("未登录异常: {}",e.getMessage());
        return AjaxBean.getNotLogin();
    }
    @ExceptionHandler(NotRoleException.class)
    AjaxBean handlerNotRoleException(NotRoleException e){
        // 如果是角色异常
        e.printStackTrace();
        log.info("角色异常: {}",e.getMessage());
        return AjaxBean.getNotJur(e.getMessage());
    }
    @ExceptionHandler(NotPermissionException.class)
    AjaxBean handlerNotPermissionException(NotPermissionException e){
        // 如果是权限异常
        e.printStackTrace();
        log.info("权限异常: {}",e.getMessage());
        return AjaxBean.getNotJur(e.getMessage());
    }
    @ExceptionHandler(DisableServiceException.class)
    AjaxBean handlerDisableLoginException(DisableServiceException e){
        // 如果是被封禁异常
        e.printStackTrace();
        log.info("封禁异常: {}",e.getMessage());
        return AjaxBean.getError(e.getMessage());
    }

    /**
     *  校验错误拦截处理
     *
     * @param e 错误信息集合
     * @return 错误信息
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    AjaxBean validationBodyException(MethodArgumentNotValidException e) {
        e.printStackTrace();
        //对校验错误信息进行封装，并输出到日志
        BindingResult result = e.getBindingResult();
        StringBuilder errorMessage = new StringBuilder();
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            errors.forEach(p ->{
                FieldError fieldError = (FieldError) p;
                log.error("数据校验错误 : object{"+fieldError.getObjectName()+"},field{"+fieldError.getField()+
                        "},errorMessage{"+fieldError.getDefaultMessage()+"}");
                errorMessage.append(fieldError.getDefaultMessage()).append("; ");
            });
        }
        return AjaxBean.getError(errorMessage.toString());
    }
    /**
     * 请求方式不支持
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    AjaxBean handleException(HttpRequestMethodNotSupportedException e) {
        e.printStackTrace();
        log.error("请求方式不支持: {}",e.getMessage(), e);
        return AjaxBean.getError(":传入请求错误不支持" + e.getMethod() + "'请求");
    }

    /**
     * 请求的数据格式错误的处理
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    AjaxBean handleException(HttpMessageNotReadableException e) {
        e.printStackTrace();
        log.error("请求的数据格式错误的处理: {}",e.getMessage(), e);
        return AjaxBean.getError("传入数据格式错误");
    }

    /**
     * 通用异常
     */
    @ExceptionHandler(RuntimeException.class)
    AjaxBean handlerRuntimeException(RuntimeException e) {
        e.printStackTrace();
        log.error("通用异常: {}",e.getMessage());
        return AjaxBean.getError(StrUtil.format("通用异常: {}",e.getMessage()));
    }

    /**
     * 顶级异常
     */
    @ExceptionHandler(Exception.class)
    AjaxBean handlerException(Exception e){
        e.printStackTrace();
        log.error("顶级异常: {}",e.getMessage());
        return AjaxBean.getError(StrUtil.format("顶级异常: {}",e.getMessage()));
    }
}
