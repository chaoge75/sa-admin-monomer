package com.wlf.server.common.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.lang.NonNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

/**
 * 非JSON中日期的入参转换，基本只要是常见的日期格式都能自动匹配上
 * 支持格式：
 * yyyy-MM-dd HH:mm:ss
 * yyyy/MM/dd HH:mm:ss
 * yyyy.MM.dd HH:mm:ss
 * yyyy年MM月dd日 HH时mm分ss秒
 * yyyy-MM-dd
 * yyyy/MM/dd
 * yyyy.MM.dd
 * HH:mm:ss
 * HH时mm分ss秒
 * yyyy-MM-dd HH:mm
 * yyyy-MM-dd HH:mm:ss.SSS
 * yyyy-MM-dd HH:mm:ss.SSSSSS
 * yyyyMMddHHmmss
 * yyyyMMddHHmmssSSS
 * yyyyMMdd
 * EEE, dd MMM yyyy HH:mm:ss z
 * EEE MMM dd HH:mm:ss zzz yyyy
 * yyyy-MM-dd'T'HH:mm:ss'Z'
 * yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
 * yyyy-MM-dd'T'HH:mm:ssZ
 * yyyy-MM-dd'T'HH:mm:ss.SSSZ
 */
public class DateConverter {
    @Configuration
    static
    class StringToDateConverter implements Converter<String, Date> {
        @Override
        public Date convert(@NonNull String s) {
            if (StrUtil.isEmpty(s)) {
                return null;
            }
            try {
                return DateUtil.parse(s).toJdkDate();
            } catch (Exception e) {
                throw new RuntimeException(StrUtil.format("日期格式错误：{},", s));
            }
        }
    }

    @Configuration
    static
    class StringToLocalDateTimeConverter implements Converter<String, LocalDateTime> {

        @Override
        public LocalDateTime convert(@NonNull String s) {
            if (StrUtil.isEmpty(s)) {
                return null;
            }
            try {
                return DateUtil.parse(s).toLocalDateTime();
            } catch (Exception e) {
                throw new RuntimeException(StrUtil.format("日期格式错误：{},", s));
            }
        }
    }

    @Configuration
    static
    class StringToLocalDateConverter implements Converter<String, LocalDate> {

        @Override
        public LocalDate convert(@NonNull String s) {
            if (StrUtil.isEmpty(s)) {
                return null;
            }
            try {
                return DateUtil.parse(s).toLocalDateTime().toLocalDate();
            } catch (Exception e) {
                throw new RuntimeException(StrUtil.format("日期格式错误：{},", s));
            }
        }
    }

    @Configuration
    static
    class StringToLocalTimeConverter implements Converter<String, LocalTime> {

        @Override
        public LocalTime convert(@NonNull String s) {
            if (StrUtil.isEmpty(s)) {
                return null;
            }
            try {
                return DateUtil.parse(s).toLocalDateTime().toLocalTime();
            } catch (Exception e) {
                throw new RuntimeException(StrUtil.format("日期格式错误：{},", s));
            }
        }
    }
}
