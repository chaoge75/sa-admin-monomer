package com.wlf.server.common.config;

import cn.hutool.system.oshi.OshiUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "file")
public class FileConfig {

    /**
     * 文件系统类型
     */
    public enum FileType {
        /**
         * 本地
         */
        local,
        /**
         * 七牛云
         */
        qiniu
    }

    /**
     * 文件系统类型目前支持本地或七牛云
     */
    public FileType fileType = FileType.qiniu;

    /**
     * 本地文件系统配置
     */
    @Getter
    @Setter
    @Configuration
    @ConfigurationProperties(prefix = "file.local")
    public static class LocalFileConfig {

        /**
         * web路径映射本地路径匹配前缀
         */
        private String webPath = "/file";
        /**
         * win路径
         */
        private String attachmentPathWin = "D:/home/sa/attachment";
        /**
         * 非Win路径，一般指Linux路径
         */
        private String attachmentPathLinux = "/home/sa/attachment";


        /**
         * 根据操作系统自动选择路径
         *
         * @return 路径
         */
        public String getAttachmentPath() {
            if (OshiUtil.getOs().getFamily().equals("Windows")) {
                return attachmentPathWin;
            } else {
                return attachmentPathLinux;
            }
        }
    }


    /**
     * 七牛云配置
     */
    @Getter
    @Setter
    @Configuration
    @ConfigurationProperties(prefix = "file.qiniu")
    public static class QiNiuFileConfig {
        /**
         * accessKey
         */
        private String accessKey = "wqnzs96GfBiE02v-hlvFkkE3-vpmm2j5YmhT3Hgw";
        /**
         * secretKey
         */
        private String secretKey = "1TOVk1j7Kz0_9NONMDLbxBybwBLsR5FeAZsdNqCX";
        /**
         * 空间名称
         */
        private String bucket = "wangijun";

        /**
         * 自定义的加速域名前缀
         */
        private String host = "http://qiniuy.wangijun.com/";
    }
}
