package com.wlf.server.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * WebSocket配置
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ws")
public class WsConfig {
    /**
     * WebSocket服务端口-不可和web服务同一个端口
     */
    private Integer port = 8001;

    /**
     * 心跳超时时间-单位-秒
     */
    private Integer heartTimeout = 10;

    /**
     * 默认匹配的路径
     */
    private String url = "/";
}
