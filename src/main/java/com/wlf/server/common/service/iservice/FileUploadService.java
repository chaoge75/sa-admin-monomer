package com.wlf.server.common.service.iservice;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.io.file.FileNameUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.wlf.server.common.entity.SysAttachment;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

/**
 * 文件管理接口，一般需要集成不同的对象存储，只需要实现这个接口即可
 */
public interface FileUploadService {
    /**
     * 上传文件
     * @param file 文件
     * @param remark 文件备注
     * @return 返回
     */
    SysAttachment plus(MultipartFile file, String remark);


    /**
     * 删除文件
     * @param id 文件Id
     */
    void del(String id);

    /**
     * 下载文件
     * @param id 文件Id
     */
    void read(String id);


    default String getPrintSize(long size) {
        // 如果字节数少于1024，则直接以B为单位，否则先除于1024，后3位因太少无意义
        if (size < 1024) {
            return size + "B";
        } else {
            size = size / 1024;
        }
        // 如果原字节数除于1024之后，少于1024，则可以直接以KB作为单位
        // 因为还没有到达要使用另一个单位的时候
        // 接下去以此类推
        if (size < 1024) {
            return size + "KB";
        } else {
            size = size / 1024;
        }
        if (size < 1024) {
            // 因为如果以MB为单位的话，要保留最后1位小数，
            // 因此，把此数乘以100之后再取余
            size = size * 100;
            return size / 100 + "." + size % 100 + "MB";
        } else {
            // 否则如果要以GB为单位的，先除于1024再作同样的处理
            size = size * 100 / 1024;
            return size / 100 + "." + size % 100 + "GB";
        }
    }

    default SysAttachment initAttachment(MultipartFile file) {
        SysAttachment attachment = new SysAttachment();
        attachment.setId(IdWorker.getIdStr());
        attachment.setOriginalFilename(file.getOriginalFilename());
        attachment.setFilename(attachment.getId() + attachment.getOriginalFilename());
        attachment.setFileType(FileNameUtil.getSuffix(attachment.getOriginalFilename()));
        attachment.setCreateTime(LocalDateTime.now());
        attachment.setCreateBy(StpUtil.getLoginIdAsString());
        attachment.setFileSize(getPrintSize(file.getSize()));
        return attachment;
    }
}
