package com.wlf.server.common.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.wlf.server.common.entity.SysApiLog;
import com.wlf.server.common.mapper.SysApiLogMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import com.wlf.server.web.util.JSON;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * <p>
 * API请求日志 服务实现类
 * </p>
 *
 * @author 清欢
 * @since 2022-09-06
 */
@Service
public class SysApiLogService extends ServiceImpl<SysApiLogMapper, SysApiLog> {

    /**
     * 列表分页数据
     * @param name 查询条件
     * @param paramPage 分页
     * @return 结果
     */
    public AjaxBean list(String name, ParamPage paramPage) {
        LambdaQueryWrapper<SysApiLog> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(name)) {
            wrapper.like(SysApiLog::getMethod, name).or();
            wrapper.like(SysApiLog::getUrl, name).or();
        }
        wrapper.orderByDesc(SysApiLog::getRequestTime);
        return AjaxBean.getOkPage(this
                .page(Page.of(paramPage.getCurrent(),
                        paramPage.getSize()), wrapper));
    }

    /**
     * 获取单个
     * @param id 主键
     * @return 数据
     */
    public AjaxBean get(String id){
        return AjaxBean.getOkData(this.getById(id));
    }

    /**
     * 删除
     * @param id 主键
     * @param ids 主键列表
     * @return 结果
     */
    public AjaxBean del(String id,String ids){
        ArrayList<String> list = new ArrayList<>();
        if (StrUtil.isNotBlank(ids)){
            list.addAll(JSON.parseArray(ids,String.class));
        } else {
            list.add(id);
        }
        return AjaxBean.getByBool(Db.removeByIds(list,this.entityClass));
    }
}
