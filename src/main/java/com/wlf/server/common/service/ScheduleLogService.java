package com.wlf.server.common.service;

import com.wlf.server.web.controller.ScheduleController;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.common.entity.ScheduleLog;
import com.wlf.server.common.mapper.ScheduleLogMapper;
@Service
public class ScheduleLogService extends ServiceImpl<ScheduleLogMapper, ScheduleLog> {

    /**
     * 删除日志
     * @param type 删除类型
     * @return 操作结果
     */
    public AjaxBean del(ScheduleController.DelType type){
        /*这里用到了JDK17的特性。和Kotlin里的when差不多*/
        return  AjaxBean.getByBool(switch (type){
            case YEAR -> this.lambdaUpdate().le(ScheduleLog::getCreateTime, LocalDateTime.now().minusYears(1)).remove();
            case YI -> this.lambdaUpdate().le(ScheduleLog::getCreateTime,LocalDateTime.now().minusMonths(1)).remove();
            case SAN -> this.lambdaUpdate().le(ScheduleLog::getCreateTime,LocalDateTime.now().minusMonths(3)).remove();
            case LIU -> this.lambdaUpdate().le(ScheduleLog::getCreateTime,LocalDateTime.now().minusMonths(6)).remove();
            case ALL -> this.lambdaUpdate().remove();
        });
    }
}
