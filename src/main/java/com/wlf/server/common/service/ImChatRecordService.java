package com.wlf.server.common.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.common.mapper.ImChatRecordMapper;
import com.wlf.server.common.entity.ImChatRecord;
@Service
public class ImChatRecordService extends ServiceImpl<ImChatRecordMapper, ImChatRecord> {

}
