package com.wlf.server.common.service;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.wlf.server.common.entity.PostUser;
import com.wlf.server.common.entity.SysPost;
import com.wlf.server.common.mapper.SysPostMapper;
import com.wlf.server.web.dto.MappingDTO;
import com.wlf.server.web.dto.PostDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@Service
public class SysPostService extends ServiceImpl<SysPostMapper, SysPost> {

    @Resource
    MappingDTO mappingDTO;

    /**
     * 主数据
     */
    public AjaxBean list(PostDTO postDTO){
        LambdaQueryWrapper<SysPost> wrapper = new LambdaQueryWrapper<>();
        List<SysPost> list = this.list(wrapper);
        TreeNodeConfig tConfig = new TreeNodeConfig();
        tConfig.setIdKey("id");
        tConfig.setChildrenKey("children");
        tConfig.setNameKey("label");

        return AjaxBean.getOkData(getTree(list,tConfig));
    }

    public AjaxBean saveOrUpdate(PostDTO postDTO){
        return AjaxBean.getByBool(Db.saveOrUpdate(mappingDTO.to(postDTO)));
    }

    public AjaxBean get(String id){
        return AjaxBean.getOkData(this.getById(id));
    }

    public AjaxBean del(String id){
        ArrayList<String> deptIds = new ArrayList<>();
        getPostIds(id,deptIds);
        // 这里需要做递归删除
        return AjaxBean.getByBool(Db.removeByIds(deptIds,this.entityClass));
    }

    public void getPostIds(String id, List<String> ids){
        ids.add(id);
        // 此处换成JDBC会更好
        List<SysPost> list = this.list(new LambdaQueryWrapper<SysPost>().eq(SysPost::getPid, id));
        if (ObjectUtil.isNotEmpty(list)){
            for (SysPost post : list) {
                getPostIds(post.getId(),ids);
            }
        }
    }

    public AjaxBean parentList(){
        List<SysPost> postList = this.list();
        SysPost post = new SysPost();
        post.setPid("0");
        post.setId("0");
        post.setName("根岗位");
        postList.add(post);
        TreeNodeConfig tConfig = new TreeNodeConfig();
        tConfig.setWeightKey("sort");
        tConfig.setIdKey("value");
        tConfig.setChildrenKey("children");
        tConfig.setNameKey("label");
        Dict dict = Dict.create();
        dict.set("pidOptions",this.getTree(postList, tConfig));
        return AjaxBean.getOkData(dict);
    }

    /**
     * 根据机构ID获取用户ID
     */
    public List<String> getUserIdsByPostIds(List<String> postIds){
        List<PostUser> organUsers = Db.lambdaQuery(PostUser.class)
                .in(PostUser::getPostId, postIds).list();
        return organUsers
                .stream().map(PostUser::getUserId).collect(Collectors.toList());
    }

    public List<PostDTO> getPostByUserId(String id){
        List<PostUser> list = Db.lambdaQuery(PostUser.class).eq(PostUser::getUserId, id).list();
        List<String> deptIds = list.stream().map(PostUser::getPostId).collect(Collectors.toList());
        List<SysPost> sysOrgans = this.listByIds(deptIds);

        return mappingDTO.toPostDTO(sysOrgans);

    }
    /**
     * 获取树形数据
     */
    public List<Tree<String>> getTree(List<SysPost> list, TreeNodeConfig tConfig){
        //转换器
        return TreeUtil.build(list, "0", tConfig,
                (l, tree) -> {
                    tree.setId(l.getId());
                    tree.setParentId(l.getPid());
                    tree.setWeight(l.getSort());
                    tree.setName(l.getName());
                    tree.putExtra("status",l.getStatus());
                });
    }
}
