package com.wlf.server.common.service;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.common.mapper.ScheduleJobMapper;
import com.wlf.server.common.entity.ScheduleJob;

@Service
public class ScheduleJobService extends ServiceImpl<ScheduleJobMapper, ScheduleJob> {

}
