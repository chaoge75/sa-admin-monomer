package com.wlf.server.common.service;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.wlf.server.common.entity.SysRegion;
import com.wlf.server.common.mapper.SysRegionMapper;
import com.wlf.server.web.dto.MappingDTO;
import com.wlf.server.web.dto.RegionDTO;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SysRegionService extends ServiceImpl<SysRegionMapper, SysRegion> {
    @Resource
    MappingDTO mappingDTO;


    public AjaxBean saveOrUpdate(RegionDTO regionDTO) {
        return AjaxBean.getByBool(Db.saveOrUpdate(mappingDTO.to(regionDTO)));
    }

    /**
     * 根据区域Id获取区域信息
     *
     * @param id 区域ID
     */
    public AjaxBean get(String id) {
        return AjaxBean.getOkData(this.getById(id));
    }


    /**
     * 递归查当前区域的所有子区域
     */
    public void getRegionIds(String id, List<String> ids) {
        ids.add(id);
        List<SysRegion> list = this.list(new LambdaQueryWrapper<SysRegion>().eq(SysRegion::getPid, id));
        if (ObjectUtil.isNotEmpty(list)) {
            for (SysRegion region : list) {
                getRegionIds(region.getId(), ids);
            }
        }
    }

    /**
     * 删除
     *
     * @param id 单个ID
     */
    public AjaxBean del(String id) {
        ArrayList<String> regionIds = new ArrayList<>();
        getRegionIds(id, regionIds);
        // 这里需要做递归删除
        return AjaxBean.getByBool(Db.removeByIds(regionIds,this.entityClass));
    }

    /**
     * 主数据
     */
    public AjaxBean list(RegionDTO regionDTO) {
        LambdaQueryWrapper<SysRegion> wrapper = new LambdaQueryWrapper<>();
        List<SysRegion> list = this.list(wrapper);
        TreeNodeConfig tConfig = new TreeNodeConfig();
        tConfig.setIdKey("id");
        tConfig.setChildrenKey("children");
        tConfig.setNameKey("label");

        return AjaxBean.getOkData(getTree(list, tConfig));
    }

    /**
     * 获取树形数据
     */
    public List<Tree<String>> getTree(List<SysRegion> list, TreeNodeConfig tConfig) {
        //转换器
        return TreeUtil.build(list, "0", tConfig,
                (l, tree) -> {
                    tree.setId(l.getId());
                    tree.setParentId(l.getPid());
                    tree.setWeight(l.getSort());
                    tree.setName(l.getName());
                    tree.putExtra("status", l.getStatus());
                });
    }

    public AjaxBean parentList() {
        List<SysRegion> regions = this.list();
        SysRegion region = new SysRegion();
        region.setPid("0");
        region.setId("0");
        region.setName("根区域");
        regions.add(region);
        TreeNodeConfig tConfig = new TreeNodeConfig();
        tConfig.setWeightKey("sort");
        tConfig.setIdKey("value");
        tConfig.setChildrenKey("children");
        tConfig.setNameKey("label");

        return AjaxBean.getOkData(this.getTree(regions, tConfig));
    }

}
