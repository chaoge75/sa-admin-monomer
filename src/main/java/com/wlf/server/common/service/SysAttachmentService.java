package com.wlf.server.common.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.common.config.FileConfig;
import com.wlf.server.common.context.BeanContext;
import com.wlf.server.common.entity.SysAttachment;
import com.wlf.server.common.mapper.SysAttachmentMapper;
import com.wlf.server.common.service.iservice.FileUploadService;
import com.wlf.server.web.dto.AttachmentDTO;
import com.wlf.server.web.util.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 附件管理
 */
@Slf4j
@Service
public class SysAttachmentService extends ServiceImpl<SysAttachmentMapper, SysAttachment> {
    @Resource
    private FileConfig fileConfig;

    /**
     * 根据配置来决定用哪个文件系统
     *
     * @return 本地文件管理或者
     */
    private FileUploadService fileUploadService() {
        return switch (fileConfig.fileType) {
            case local -> BeanContext.getBean(FileUploadServiceLocalImpl.class);
            case qiniu -> BeanContext.getBean(FileUploadServiceQiNiuImpl.class);
        };
    }

    /**
     * 附件信息列表分页数据
     */
    public Page<SysAttachment> page(AttachmentDTO attachmentDTO, Page<SysAttachment> page) {
        LambdaQueryWrapper<SysAttachment> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(attachmentDTO.getFilename())) {
            String key = attachmentDTO.getFilename();
            wrapper.like(SysAttachment::getOriginalFilename, key).or();
            wrapper.like(SysAttachment::getFileType, key).or();
            wrapper.like(SysAttachment::getRemark, key);
        }
        wrapper.orderByDesc(SysAttachment::getCreateTime);
        page = this.page(page, wrapper);
        return page;
    }

    /**
     * 上传文件
     *
     * @param file   文件
     * @param remark 文件上传备注
     * @return 文件详细信息
     */
    public SysAttachment plus(MultipartFile file, String remark) {
        SysAttachment attachment = fileUploadService().plus(file, remark);
        this.save(attachment);
        return attachment;
    }

    /**
     * 默认文件上传
     *
     * @param file 文件
     * @return 文件详细信息
     */
    public SysAttachment plus(MultipartFile file) {
        return plus(file, "默认上传");
    }


    /**
     * 根据ID读取文件（下载）
     */
    public void read(String id) {
        fileUploadService().read(id);
    }

    /**
     * 根据ID获取附件信息
     */
    public SysAttachment get(String id) {
        return this.getById(id);
    }

    /**
     * 根据ID删除附件信息包含文件
     */
    public void del(String id, String ids) {
        if (StrUtil.isNotBlank(ids)) {
            List<String> idArr = JSON.parseArray(ids, String.class);
            for (String i : idArr) {
                fileUploadService().del(i);
            }
            BeanContext.getBean(this.getClass()).removeBatchByIds(idArr);
            return;
        }
        fileUploadService().del(id);
        this.removeById(id);
    }
}

