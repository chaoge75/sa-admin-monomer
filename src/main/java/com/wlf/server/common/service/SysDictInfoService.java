package com.wlf.server.common.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.common.entity.SysDictInfo;
import com.wlf.server.common.mapper.SysDictInfoMapper;
import org.springframework.stereotype.Service;
@Service
public class SysDictInfoService extends ServiceImpl<SysDictInfoMapper, SysDictInfo> {

}
