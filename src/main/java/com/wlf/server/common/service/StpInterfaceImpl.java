package com.wlf.server.common.service;

import cn.dev33.satoken.stp.StpInterface;
import com.wlf.server.web.dto.MenuDTO;
import com.wlf.server.web.dto.RoleDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 自定义权限验证接口扩展
 */
@Slf4j
@Component
public class StpInterfaceImpl implements StpInterface {
    @Resource
    SysUserService userService;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        log.info("进入getPermissionList");
        return userService.getUser(loginId.toString()).getMenuList()
                .stream().map(MenuDTO::getPermission).toList();
    }


    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        log.info("进入getRoleList");
        return userService.getUser(loginId.toString()).getRoleList()
                .stream().map(RoleDTO::getCode).toList();
    }
}
