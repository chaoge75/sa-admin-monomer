package com.wlf.server.common.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.wlf.server.common.context.BeanContext;
import com.wlf.server.common.em.DictKey;
import com.wlf.server.common.entity.SysDict;
import com.wlf.server.common.entity.SysDictInfo;
import com.wlf.server.common.mapper.SysDictMapper;
import com.wlf.server.web.dto.DictDTO;
import com.wlf.server.web.dto.DictInfoDTO;
import com.wlf.server.web.dto.MappingDTO;
import com.wlf.server.web.dto.ParamPage;
import com.wlf.server.web.util.AjaxBean;
import com.wlf.server.web.util.JSON;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wlf.server.common.em.CacheKey.dict;

@Service
@CacheConfig(cacheNames = dict)
public class SysDictService extends ServiceImpl<SysDictMapper, SysDict> {
    @Resource
    private MappingDTO mappingDTO;
    @Resource
    private SysDictInfoService dictInfoService;


    /**
     * 获取字典的字典值集合
     * 一般用作查询条件
     *
     * @param key 字典编码
     * @return 集合
     */
    @Cacheable(key = "#key")
    public List<SysDictInfo> getList(DictKey key) {
        return dictInfoService.lambdaQuery()
                .eq(SysDictInfo::getDictCode, key.name())
                .orderByAsc(SysDictInfo::getSort)
                .list();
    }

    /**
     * 获取字典值的Map
     * 一般用来赋值
     *
     * @param key 字典编码
     * @return map
     */
    public Map<String, String> getMap(DictKey key) {
        List<SysDictInfo> list = BeanContext.getBean(this.getClass()).getList(key);
        HashMap<String, String> map = new HashMap<>();
        for (SysDictInfo entry : list) {
            map.put(entry.getValue(), entry.getLabel());
        }
        return map;
    }


    /**
     * 保存或更新字典数据
     *
     * @param dict 数据字符串
     * @return 结果
     */
    @CacheEvict(allEntries = true)
    public AjaxBean saveOrUpdate(DictDTO dict) {
        return AjaxBean.getByBool(Db.saveOrUpdate(mappingDTO.to(dict)));
    }

    /**
     * 获取字典
     *
     * @param id 字典ID
     * @return 数据
     */
    @Cacheable(key = "#id")
    public AjaxBean get(String id) {
        return AjaxBean.getOkData(this.getById(id));
    }

    /**
     * 主列表数据
     *
     * @param dto       查询参数
     * @param paramPage 分页
     * @return 数据
     */
    public AjaxBean list(DictDTO dto, ParamPage paramPage) {
        LambdaQueryWrapper<SysDict> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(dto.getName())) {
            wrapper.like(SysDict::getName, dto.getName()).or();
            wrapper.like(SysDict::getCode, dto.getName());
        }
        wrapper.orderByDesc(SysDict::getId);
        return AjaxBean.getOkPage(this.page(Page.of(paramPage.getCurrent(), paramPage.getSize()), wrapper));
    }

    /**
     * 单个或批量删除
     *
     * @param id  单个Id
     * @param ids 批量删除ID字符串
     * @return 结果
     */
    @CacheEvict(allEntries = true)
    public AjaxBean del(String id, String ids) {
        if (StrUtil.isBlank(ids)) {
            return AjaxBean.getByBool(this.removeById(id));
        } else {
            return AjaxBean.getByBool(Db.removeByIds(JSON.parseArray(ids, String.class), this.entityClass));
        }
    }


    /**
     * 保存或更新字典数据
     *
     * @param dict 数据字符串
     * @return 结果
     */
    @CacheEvict(allEntries = true)
    public AjaxBean infoSaveOrUpdate(DictInfoDTO dict) {
        return AjaxBean.getByBool(dictInfoService.saveOrUpdate(mappingDTO.to(dict)));
    }

    /**
     * 获取字典
     *
     * @param id 字典ID
     * @return 数据
     */
    public AjaxBean infoGet(String id) {
        return AjaxBean.getOkData(dictInfoService.getById(id));
    }

    /**
     * 主列表数据
     *
     * @param dto       查询参数
     * @param paramPage 分页
     * @return 数据
     */
    public AjaxBean infoList(DictInfoDTO dto, ParamPage paramPage) {
        LambdaQueryWrapper<SysDictInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictInfo::getDictCode, dto.getDictCode());
        if (StrUtil.isNotBlank(dto.getLabel())) {
            wrapper.like(SysDictInfo::getLabel, dto.getLabel()).or();
            wrapper.like(SysDictInfo::getValue, dto.getLabel());
        }
        wrapper.orderByAsc(SysDictInfo::getSort);
        return AjaxBean.getOkPage(dictInfoService.page(Page.of(paramPage.getCurrent(), paramPage.getSize()), wrapper));
    }

    /**
     * 单个或批量删除
     *
     * @param id  单个Id
     * @param ids 批量删除ID字符串
     * @return 结果
     */
    @CacheEvict(allEntries = true)
    public AjaxBean infoDel(String id, String ids) {
        if (StrUtil.isBlank(ids)) {
            return AjaxBean.getByBool(dictInfoService.removeById(id));
        } else {
            return AjaxBean.getByBool(dictInfoService.removeBatchByIds(JSON.parseArray(ids, String.class)));
        }
    }
}
