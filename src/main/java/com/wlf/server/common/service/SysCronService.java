package com.wlf.server.common.service;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlf.server.common.mapper.SysCronMapper;
import com.wlf.server.common.entity.SysCron;
@Service
public class SysCronService extends ServiceImpl<SysCronMapper, SysCron> {

}
