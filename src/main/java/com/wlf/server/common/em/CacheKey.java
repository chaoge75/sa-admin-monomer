package com.wlf.server.common.em;

/**
 * 缓存的名称吧
 */
public class CacheKey {

    public final static String user = "user";
    public final static String role = "role";
    public final static String menu = "menu";
    public final static String power = "power";
    public final static String notice = "notice";

    public final static String dict = "dict";
}
