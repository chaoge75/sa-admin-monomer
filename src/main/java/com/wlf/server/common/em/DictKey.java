package com.wlf.server.common.em;



/**
 * 对应系统里的字典编码,这个东西我建议不要纯使用魔法值，因为
 * 字典和系统是强耦合的，如果增加了字典值,建议也在代码里同步增加一下字典编码
 */
public enum DictKey {
    /**
     * 机构类型
     */
    organ_type,

    /**
     * 房屋类型
     */
    house_type,



}


