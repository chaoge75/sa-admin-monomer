package com.wlf.server.common.em;

/**
 * 常量枚举在一起的类
 */
public class Const {

    /**
     * 定时任务Key的名称
     */
    public final static String TaskBeanKey = "TASK_";

    /**
     * 登录设备
     */
    public enum LoginType{
        PC
    }

    /**
     * 菜单类型，菜单就是显示菜单，按钮对应的是后端的接口
     */
    public enum MenuType {
        MENU(1,"菜单"),
        BUTTON(2,"权限");
        private final Integer id;
        private final String name;

        MenuType(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }
    }

    /**
     * 自定义序列
     */
    public enum SerialType {
        // 登陆序号
        LOGIN_NO(4, RestType.NONE);

        // 序列长度，长度不够补零
        final Integer length;
        // 重置类别
        final RestType restType;

        SerialType(Integer length, RestType restType) {
            this.length = length;
            this.restType = restType;
        }

        public Integer getLength() {
            return length;
        }

        public RestType getType() {
            return restType;
        }

        public enum RestType {
            //  每天重置。。。。最后一个为不重置；
            // 每天重置或者每月，每年重置等，作为主键时前面需要带上相应的日期，不然，就没有不然了。
            // 就是如果是每年重置主键应该是Year+Serial
            // 就是如果是每月重置主键应该是Year+Month+Serial
            // 就是如果是每天重置主键应该是Year+Month+Day+Serial
            // 设置不重置时，序列长度最好设置长点。不然可能会不够用。
            DAY("yyyy-MM-dd"),MONTH("yyyy-MM"),YEAR("yyyy"),NONE("");

            private final String format;

            RestType(String format) {
                this.format = format;
            }

            public String getFormat() {
                return format;
            }
        }
    }

    /**
     * 用户类型
     */
    public enum UserType {
        // 系统用户，普通用户
        SYS("系统用户"),NORM("正常用户"),BANK("银行用户")
        ;
        final String desc;

        UserType(String name) {
            this.desc = name;
        }

        public String getDesc() {
            return desc;
        }
    }

    /**
     * 通知状态
     */
    public enum NoticeType{
        todo("草稿"),
        release("已发布"),
        ;

        NoticeType(String info) {
            this.info = info;
        }
        private final String info;

        public String getInfo() {
            return info;
        }
    }
}
