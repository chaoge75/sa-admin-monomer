package com.wlf.server.common.ws;


import com.wlf.server.web.util.JSON;

import java.io.Serial;
import java.io.Serializable;
import java.util.LinkedHashMap;


/**
 * WebSocket返回载体
 */
public class WsBean extends LinkedHashMap<String, Object> implements Serializable {

    /**
     * 指定调用前端的回调函数
     */
    public enum CallBackEm {
        // 通知回调函数
        notice,
        // 收到消息的回调
        receive_msg,
    }

    @Serial
    private static final long serialVersionUID = 1L;    // 序列化版本号

    public WsBean(CallBackEm callBackEm, Object data){
        super();
        this.put("code",callBackEm.name());
        this.put("data",data);
    }
    public static WsBean get(CallBackEm em){
        return new WsBean(em,"");
    }
    public static WsBean get(CallBackEm em, Object data) {
        return new WsBean(em,data);
    }

    public WsBean setData(Object data){
        this.put("data",data);
        return this;
    }

    public WsBean set(String key, Object value){
        this.put(key,value);
        return this;
    }
    public String toJson(){
        return JSON.toJSONString(this);
    }
}
