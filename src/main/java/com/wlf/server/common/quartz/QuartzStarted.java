package com.wlf.server.common.quartz;


import com.wlf.server.common.context.BeanContext;
import com.wlf.server.common.entity.ScheduleJob;
import com.wlf.server.common.mapper.ScheduleJobMapper;
import org.quartz.CronTrigger;
import org.quartz.Scheduler;

import java.util.List;

/**
 * Quartz Started
 * <p>
 * Spring 初始化时检测任务, 加载 Job 任务到内存
 *
 * @serial 2.0.0
 * @author 就眠儀式
 */
public class QuartzStarted {

    /**
     * 定时任务初始化
     */
    public static void init() {
        ScheduleJobMapper scheduleJobMapper = BeanContext.getBean(ScheduleJobMapper.class);
        Scheduler scheduler = BeanContext.getBean(Scheduler.class);
        List<ScheduleJob> scheduleJobList = scheduleJobMapper.selectList(null);
        for (ScheduleJob scheduleJob : scheduleJobList) {
            CronTrigger cronTrigger = QuartzService.getCronTrigger(scheduler, Long.parseLong(scheduleJob.getId()));
            if (cronTrigger == null) {
                QuartzService.createJob(scheduler, scheduleJob);
            } else {
                QuartzService.updateJob(scheduler, scheduleJob);
            }
        }
    }
}
