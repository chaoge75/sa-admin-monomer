package com.wlf.server.common.quartz;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.wlf.server.common.context.BeanContext;
import com.wlf.server.common.entity.ScheduleJob;
import com.wlf.server.common.entity.ScheduleLog;
import com.wlf.server.common.service.ScheduleLogService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * Quartz Job 执行逻辑
 * <p>
 * 通过 className 反射生成实例, 执行任务并记录日志
 *
 * @serial 2.0.0
 * @author 就眠儀式
 */
@Slf4j
public class QuartzExecute extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) {
        Object o = context.getMergedJobDataMap().get(ScheduleJob.JOB_PARAM_KEY);
        ScheduleJob jobBean = (ScheduleJob) o;
        ScheduleLogService logService = BeanContext.getBean(ScheduleLogService.class);
        ScheduleLog logBean = new ScheduleLog();
        logBean.setId(IdWorker.getIdStr());
        logBean.setJobId(jobBean.getId());
        logBean.setBeanName(jobBean.getBeanName());
        logBean.setParams(jobBean.getParams());
        logBean.setCreateTime(LocalDateTime.now());
        long beginTime = System.currentTimeMillis();
        try {
            Object target = BeanContext.getBean(jobBean.getBeanName());
            Method method = target.getClass().getDeclaredMethod("run", String.class);
            method.invoke(target, jobBean.getParams());
            long executeTime = System.currentTimeMillis() - beginTime;
            logBean.setTimes((int) executeTime);
            logBean.setStatus(0);
            log.info("定时器 === >> " + jobBean.getName() + "执行成功,耗时 === >> " + executeTime);
        } catch (Exception e) {
            long executeTime = System.currentTimeMillis() - beginTime;
            logBean.setTimes((int) executeTime);
            logBean.setStatus(1);
            logBean.setError(e.getMessage());
            e.getCause();
        } finally {
            logService.save(logBean);
        }
    }
}
