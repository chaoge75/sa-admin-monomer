package com.wlf.server.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.wlf.server.web.util.AjaxBean;
import com.wlf.server.web.util.JSON;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/generator")
public class GeneratorController {

    @Resource
    private GeneratorService generatorService;

    /**
     * 主数据
     * @param name 表名
     * @return data
     */
    @GetMapping("/list")
    AjaxBean list(String name){
        return generatorService.list(name);
    }

    /**
     * 全局配置
     */
    @Getter
    @Setter
    static class GlobalConfig{
        /**
         * 作者名
         */
        private String authorName;

        /**
         * 是否开启 swagger 模式
         */
        private Boolean enableSwagger;

        /**
         * 父包名
         */
        private String entityName;
        private String serviceName;
        private String serviceImplName;
        private String mapperName;
        private String xmlName;
        private String controllerName;
        private String serviceFileName;
        private String serviceImplFilename;

        /**
         * 启用模板,如果List里面有的则生成，反之没有则禁用
         */
        private List<String> tempList;

        /**
         * 开启大写命名
         */
        private Boolean enableCapitalMode;

        /**
         * 过滤表前缀
         */
        private String addTablePrefix;


        /**
         * 主键类型
         */
        private IdType idType;


        /**
         * 是否开启Lombok
         */
        private Boolean enableLombok;

        /**
         * 开启 @Mapper 注解
         */
        private Boolean enableMapperAnnotation;
    }

    @PostMapping("/generator")
    void generator(String config,String tables){
        GlobalConfig globalConfig = JSON.parseObject(config, GlobalConfig.class);
        List<String> tablesList = JSON.parseArray(tables, String.class);
        generatorService.generator(globalConfig,tablesList);
    }

    /**
     * 表单构建初始化
     * @return 包名集合
     */
    @GetMapping("/init-form")
    AjaxBean initForm(){
        return generatorService.initForm();
    }

    /**
     * 查看表字段信息
     * @param name 表名
     * @return 字段信息
     */
    @GetMapping("/show-table-field")
    AjaxBean showTableField(String name){
        return generatorService.showTable(name);
    }
}
