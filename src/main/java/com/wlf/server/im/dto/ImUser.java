package com.wlf.server.im.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ImUser implements Serializable {

    /**
     * 主键
     */
    private String id;

    /**
     * 用户名
     */
    private String name;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 登录账号
     */
    private String loginNo;

    /**
     * 手机号码
     */
    private String phone;


    /**
     * 性别，0男，1女，2未知
     */
    private Integer sex;

    /**
     * 备注
     */
    private String remark;


    /**
     * 最后登陆时间
     */
    private LocalDateTime loginEndTime;

    /**
     * 用户状态
     */
    private ImConst.UserStatus userStatus;

    /**
     * 最后消息显示时间
     */
    private String lastSendTime;

    /**
     * 最后一条消息记录内容
     */
    private String lastMsg;

    /**
     * 未读消息数量
     */
    private Integer noReadNum;


    @Serial
    private static final long serialVersionUID = 1L;
}
