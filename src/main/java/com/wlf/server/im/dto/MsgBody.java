package com.wlf.server.im.dto;


import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class MsgBody implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    public enum MsgType{
        // 普通文字消息
        text,
        // 图片消息
        img,
    }
    public enum Type{
        // 自己
        self,
        // 别人
        other,
    }

    /**
     * 唯一ID
     */
    private String id;

    /**
     * 消息是自己发的还是收到的消息
     */
    private Type type;

    /**
     * 消息类别
     */
    private MsgType msgType;

    /**
     * 消息主体（图片消息时主体就是url）
     */
    private String msgContent;
}
