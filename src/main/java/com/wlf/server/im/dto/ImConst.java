package com.wlf.server.im.dto;

public class ImConst {
    /**
     * 用户状态常量
     */
    public enum UserStatus {
        // 在线
        online,
        // 离线
        leave,
    }


}
