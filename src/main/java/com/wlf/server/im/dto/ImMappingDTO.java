package com.wlf.server.im.dto;

import com.wlf.server.common.entity.SysUser;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ImMappingDTO {
    ImUser toImUser(SysUser user);

    List<ImUser> toImUser(List<SysUser> users);

}
