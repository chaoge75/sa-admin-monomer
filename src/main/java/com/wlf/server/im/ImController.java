package com.wlf.server.im;

import com.wlf.server.im.dto.MsgBody;
import com.wlf.server.web.util.AjaxBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 起个头吧，在线聊天功能必须要写啊
 * 在线聊天
 */
@RestController
@RequestMapping("/im")
public class ImController {
    @Resource
    private ImService imService;

    /**
     * 在线用户列表
     *
     * @return 数据
     */
    @GetMapping("/online")
    AjaxBean online() {
        return imService.online();
    }

    /**
     * 向指定用户发送数据
     *
     * @param userId 用户ID
     * @param msg    信息主体
     * @return 发送 结果
     */
    @PostMapping("/send")
    AjaxBean send(String userId, String msg) {
        return imService.send(userId, msg, MsgBody.MsgType.text);
    }

    /**
     * 发送历史图片
     *
     * @param userId 发给谁
     * @param msg    图片主体
     * @return 消息主体
     */
    @PostMapping("/send-img")
    AjaxBean sendImg(String userId, String msg) {
        return imService.send(userId, msg, MsgBody.MsgType.img);
    }

    /**
     * 上传并发送图片
     *
     * @param file   图片文件
     * @param userId 用户ID
     * @return 发送结果
     */
    @PostMapping("/upload-send-img")
    AjaxBean uploadSendImg(MultipartFile file, String userId) {
        return imService.uploadSendImg(file, userId);
    }

    /**
     * 获取聊天记录
     *
     * @param userId 和谁的聊天记录
     * @return 记录
     */
    @GetMapping("/user-click")
    AjaxBean userClick(String userId) {
        return imService.userClick(userId);
    }
}
